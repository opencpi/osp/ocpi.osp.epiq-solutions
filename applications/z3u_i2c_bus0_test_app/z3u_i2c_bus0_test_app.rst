.. z3u_i2c_bus0_test_app application

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _z3u_i2c_bus0_test_app:

app: ``z3u_i2c_bus0_test_app``
==========================

Description
-----------

Tests the access of the various I2C devices of the "bus0" of the Z3u. Generally, reading of the
device worker properties is performed, but writing of some properties may also be performed.

This document assumes that the user has reviewed the datasheets for the RCC and HDL workers that
are present in this application (icm20602).

**icm20602**: This worker in the application allows the user to initialize the component and set the
Gyroscope Sensitivity (input) and Accelerometer Sensitivity (input) property values. Depending on
the input property values, the component will return the Gyroscopic Data (output) and Accelerometer
Data (output) property values. This component also returns the Temperature of the device. Furthermore,
this component supports a ``continunous data`` mode which continuously returns gyroscopic and
accelerometer data property values. In this mode, the user can observe these property values changing
while physically changing the location and orientation of the Z3u.

**eeprom_24AA128**: This component is **NOT** included in the application. Work was initially
done to include the EEPROM 24AA128 but an I2C addressing bug in the FOSS limits
the ability to read I2C addresses > 8 bits. Read more at OpenCPI Ticket: #2662.

Hardware Portablity
-------------------

The I2C devices workers implemented on this bus are not unique to the Z3u and it is possible
to reuse on other platforms. However, the subdevice worker use here is not likely portable,
therefore a new one for the target platform will need to be created.

Prerequisites
-------------

The ``z3u`` has been properly installed and deployed, and the application assembly
bitstream is built. Specifically, this test application requires that the following
assets are built:

  * HDL Assembly: ``empty`` with container ``cnt_z3u_i2c_bus0.xml`` (from the Z3u OSP)
  * RCC Proxy: ``icm20602_proxy.rcc`` (from the Z3u OSP)

Execution
---------

.. note::

   The instructions are written for **Network Mode**

#. Boot the Matchstiq Z3U, setup for Network Mode, then browse to the following directory:

   % ``cd /mnt/ocpi_z3u/applications/z3u_i2c_bus0_test_app/``

#. Setup the artifact search path:

   % ``export OCPI_LIBRARY_PATH=../../artifacts``

#. Run the application

   #. In non-``continuous data`` mode

      % ``ocpirun -v -d -t 1 z3u_i2c_bus0_test_app.xml``

   #. In ``continuous data`` mode, where the Gyroscope and Accelerometer x,y,z fields
      change with the movements of the Z3u:

      % ``ocpirun -v -d continuous_data_mode.xml``

      .. warning::

	 When ran under Server Mode, ``continuous data`` mode does not return property values
	 the Host terminal. Only Standalone and Network modes provide the expected results.


Example of stdout for non-``continuous data`` mode
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block::

   Available containers are:  0: PL:0 [model: hdl os:  platform: z3u], 1: rcc0 [model: rcc os: linux platform: xilinx18_3_aarch64]
   Actual deployment is:
   Instance  0 icm20602_proxy (spec ocpi.osp.epiq_solutions.devices.icm20602_proxy) on rcc container 1: rcc0, using icm20602_proxy in ../../artifacts/ocpi.osp.epiq_solutions.devices.icm20602_proxy.rcc.0.xilinx18_3_aarch64.so dated Tue Dec  7 17:32:43 2021
   Instance  1 icm20602_proxy.icm20602 (spec ocpi.osp.epiq_solutions.devices.icm20602) on hdl container 0: PL:0, using icm20602/c/icm20602 in ../../artifacts/ocpi.osp.epiq_solutions.empty_z3u_base_cnt_z3u_i2c_bus0.hdl.0.z3u.bitz dated Fri Jan  7 20:08:00 2022
   Application XML parsed and deployments (containers and artifacts) chosen [12 s 932 ms]
   Application established: containers, workers, connections all created [0 s 484 ms]
   Dump of all initial property values:
   Property   0: icm20602_proxy.init_config = "true"
   Property   1: icm20602_proxy.continuous_data = "false"
   Property   2: icm20602_proxy.gyro_sensitivity = "250"
   Property   3: icm20602_proxy.accel_sensitivity = "2"
   Property   4: icm20602_proxy.gyro_data = "x -1,y -1,z 1"
   Property   5: icm20602_proxy.accel_data = "x 0,y 0,z 1"
   Property   6: icm20602_proxy.temp_data = "39"
   Property  10: icm20602_proxy.icm20602.pad00 = "0,0,0,225"
   Property  11: icm20602_proxy.icm20602.xg_offs_tc_h = "255"
   Property  12: icm20602_proxy.icm20602.xg_offs_tc_l = "255"
   Property  13: icm20602_proxy.icm20602.pad01 = "253"
   Property  14: icm20602_proxy.icm20602.yg_offs_tc_h = "248"
   Property  15: icm20602_proxy.icm20602.yg_offs_tc_l = "13"
   Property  16: icm20602_proxy.icm20602.pad02 = "228"
   Property  17: icm20602_proxy.icm20602.zg_offs_tc_h = "255"
   Property  18: icm20602_proxy.icm20602.zg_offs_tc_l = "253"
   Property  19: icm20602_proxy.icm20602.pad03 = "0"
   Property  20: icm20602_proxy.icm20602.self_test_x_accel = "89"
   Property  21: icm20602_proxy.icm20602.self_test_y_accel = "88"
   Property  22: icm20602_proxy.icm20602.self_test_z_accel = "118"
   Property  23: icm20602_proxy.icm20602.pad04 = "179,170,14"
   Property  24: icm20602_proxy.icm20602.xg_offs_usrh = "0"
   Property  25: icm20602_proxy.icm20602.xg_offs_usrl = "0"
   Property  26: icm20602_proxy.icm20602.yg_offs_usrh = "0"
   Property  27: icm20602_proxy.icm20602.yg_offs_usrl = "0"
   Property  28: icm20602_proxy.icm20602.zg_offs_usrh = "0"
   Property  29: icm20602_proxy.icm20602.zg_offs_usrl = "0"
   Property  30: icm20602_proxy.icm20602.smplrt_div = "0"
   Property  31: icm20602_proxy.icm20602.config = "128"
   Property  32: icm20602_proxy.icm20602.gyro_config = "0"
   Property  33: icm20602_proxy.icm20602.accel_config_1 = "0"
   Property  34: icm20602_proxy.icm20602.accel_config_2 = "0"
   Property  35: icm20602_proxy.icm20602.lp_mode_cfg = "0"
   Property  36: icm20602_proxy.icm20602.pad05 = "0"
   Property  37: icm20602_proxy.icm20602.accel_wom_x_thr = "0"
   Property  38: icm20602_proxy.icm20602.accel_wom_y_thr = "0"
   Property  39: icm20602_proxy.icm20602.accel_wom_z_thr = "0"
   Property  40: icm20602_proxy.icm20602.fifo_en = "0"
   Property  41: icm20602_proxy.icm20602.pad06 = "0"
   Property  42: icm20602_proxy.icm20602.fsync_int = "0"
   Property  43: icm20602_proxy.icm20602.int_pin_cfg = "0"
   Property  44: icm20602_proxy.icm20602.int_enable = "0"
   Property  45: icm20602_proxy.icm20602.fifo_wm_int_status = "0"
   Property  46: icm20602_proxy.icm20602.int_status = "1"
   Property  47: icm20602_proxy.icm20602.accel_xout_h = "0"
   Property  48: icm20602_proxy.icm20602.accel_xout_l = "156"
   Property  49: icm20602_proxy.icm20602.accel_yout_h = "0"
   Property  50: icm20602_proxy.icm20602.accel_yout_l = "80"
   Property  51: icm20602_proxy.icm20602.accel_zout_h = "76"
   Property  52: icm20602_proxy.icm20602.accel_zout_l = "16"
   Property  53: icm20602_proxy.icm20602.temp_out_h = "18"
   Property  54: icm20602_proxy.icm20602.temp_out_l = "48"
   Property  55: icm20602_proxy.icm20602.gyro_xout_h = "255"
   Property  56: icm20602_proxy.icm20602.gyro_xout_l = "169"
   Property  57: icm20602_proxy.icm20602.gyro_yout_h = "255"
   Property  58: icm20602_proxy.icm20602.gyro_yout_l = "183"
   Property  59: icm20602_proxy.icm20602.gyro_zout_h = "0"
   Property  60: icm20602_proxy.icm20602.gyro_zout_l = "55"
   Property  61: icm20602_proxy.icm20602.pad07 = "0"
   Property  62: icm20602_proxy.icm20602.self_test_x_gyro = "193"
   Property  63: icm20602_proxy.icm20602.self_test_y_gyro = "197"
   Property  64: icm20602_proxy.icm20602.self_test_z_gyro = "207"
   Property  65: icm20602_proxy.icm20602.pad08 = "165,142,205,2,30,9,144,38,0"
   Property  66: icm20602_proxy.icm20602.fifo_wm_th1 = "0"
   Property  67: icm20602_proxy.icm20602.fifo_wm_th2 = "0"
   Property  68: icm20602_proxy.icm20602.pad09 = "0,28,0"
   Property  69: icm20602_proxy.icm20602.signal_path_reset = "0"
   Property  70: icm20602_proxy.icm20602.accel_intel_ctrl = "0"
   Property  71: icm20602_proxy.icm20602.user_ctrl = "0"
   Property  72: icm20602_proxy.icm20602.pwr_mgmt_1 = "1"
   Property  73: icm20602_proxy.icm20602.pwr_mgmt_2 = "0"
   Property  74: icm20602_proxy.icm20602.pad10 = "0"
   Property  75: icm20602_proxy.icm20602.i2c_if = "0"
   Property  76: icm20602_proxy.icm20602.pad11 = "0"
   Property  77: icm20602_proxy.icm20602.fifo_counth = "0"
   Property  78: icm20602_proxy.icm20602.fifo_countl = "0"
   Property  79: icm20602_proxy.icm20602.fifo_r_w = "127"
   Property  80: icm20602_proxy.icm20602.who_am_i = "18"
   Property  81: icm20602_proxy.icm20602.pad12 = "0"
   Property  82: icm20602_proxy.icm20602.xa_offset_h = "2"
   Property  83: icm20602_proxy.icm20602.xa_offset_l = "184"
   Property  84: icm20602_proxy.icm20602.pad13 = "0"
   Property  85: icm20602_proxy.icm20602.ya_offset_h = "253"
   Property  86: icm20602_proxy.icm20602.ya_offset_l = "22"
   Property  87: icm20602_proxy.icm20602.pad14 = "0"
   Property  88: icm20602_proxy.icm20602.za_offset_h = "252"
   Property  89: icm20602_proxy.icm20602.za_offset_l = "238"
   Application started/running [0 s 295 ms]
   [1 s 5 ms]
   Dump of all final property values:
   Property   0: icm20602_proxy.init_config = "true"
   Property   1: icm20602_proxy.continuous_data = "false"
   Property   2: icm20602_proxy.gyro_sensitivity = "250"
   Property   3: icm20602_proxy.accel_sensitivity = "2"
   Property   4: icm20602_proxy.gyro_data = "x 0,y 0,z 0"
   Property   5: icm20602_proxy.accel_data = "x 0,y 0,z 1"
   Property   6: icm20602_proxy.temp_data = "39"
   Property  11: icm20602_proxy.icm20602.xg_offs_tc_h = "255"
   Property  12: icm20602_proxy.icm20602.xg_offs_tc_l = "255"
   Property  14: icm20602_proxy.icm20602.yg_offs_tc_h = "248"
   Property  15: icm20602_proxy.icm20602.yg_offs_tc_l = "13"
   Property  17: icm20602_proxy.icm20602.zg_offs_tc_h = "255"
   Property  18: icm20602_proxy.icm20602.zg_offs_tc_l = "253"
   Property  20: icm20602_proxy.icm20602.self_test_x_accel = "89"
   Property  21: icm20602_proxy.icm20602.self_test_y_accel = "88"
   Property  22: icm20602_proxy.icm20602.self_test_z_accel = "118"
   Property  24: icm20602_proxy.icm20602.xg_offs_usrh = "0"
   Property  25: icm20602_proxy.icm20602.xg_offs_usrl = "0"
   Property  26: icm20602_proxy.icm20602.yg_offs_usrh = "0"
   Property  27: icm20602_proxy.icm20602.yg_offs_usrl = "0"
   Property  28: icm20602_proxy.icm20602.zg_offs_usrh = "0"
   Property  29: icm20602_proxy.icm20602.zg_offs_usrl = "0"
   Property  30: icm20602_proxy.icm20602.smplrt_div = "0"
   Property  31: icm20602_proxy.icm20602.config = "128"
   Property  32: icm20602_proxy.icm20602.gyro_config = "0"
   Property  33: icm20602_proxy.icm20602.accel_config_1 = "0"
   Property  34: icm20602_proxy.icm20602.accel_config_2 = "0"
   Property  35: icm20602_proxy.icm20602.lp_mode_cfg = "0"
   Property  37: icm20602_proxy.icm20602.accel_wom_x_thr = "0"
   Property  38: icm20602_proxy.icm20602.accel_wom_y_thr = "0"
   Property  39: icm20602_proxy.icm20602.accel_wom_z_thr = "0"
   Property  40: icm20602_proxy.icm20602.fifo_en = "0"
   Property  42: icm20602_proxy.icm20602.fsync_int = "0"
   Property  43: icm20602_proxy.icm20602.int_pin_cfg = "0"
   Property  44: icm20602_proxy.icm20602.int_enable = "0"
   Property  45: icm20602_proxy.icm20602.fifo_wm_int_status = "0"
   Property  46: icm20602_proxy.icm20602.int_status = "1"
   Property  47: icm20602_proxy.icm20602.accel_xout_h = "0"
   Property  48: icm20602_proxy.icm20602.accel_xout_l = "128"
   Property  49: icm20602_proxy.icm20602.accel_yout_h = "0"
   Property  50: icm20602_proxy.icm20602.accel_yout_l = "36"
   Property  51: icm20602_proxy.icm20602.accel_zout_h = "76"
   Property  52: icm20602_proxy.icm20602.accel_zout_l = "0"
   Property  53: icm20602_proxy.icm20602.temp_out_h = "18"
   Property  54: icm20602_proxy.icm20602.temp_out_l = "96"
   Property  55: icm20602_proxy.icm20602.gyro_xout_h = "255"
   Property  56: icm20602_proxy.icm20602.gyro_xout_l = "194"
   Property  57: icm20602_proxy.icm20602.gyro_yout_h = "255"
   Property  58: icm20602_proxy.icm20602.gyro_yout_l = "204"
   Property  59: icm20602_proxy.icm20602.gyro_zout_h = "0"
   Property  60: icm20602_proxy.icm20602.gyro_zout_l = "52"
   Property  62: icm20602_proxy.icm20602.self_test_x_gyro = "193"
   Property  63: icm20602_proxy.icm20602.self_test_y_gyro = "197"
   Property  64: icm20602_proxy.icm20602.self_test_z_gyro = "207"
   Property  66: icm20602_proxy.icm20602.fifo_wm_th1 = "0"
   Property  67: icm20602_proxy.icm20602.fifo_wm_th2 = "0"
   Property  69: icm20602_proxy.icm20602.signal_path_reset = "0"
   Property  70: icm20602_proxy.icm20602.accel_intel_ctrl = "0"
   Property  71: icm20602_proxy.icm20602.user_ctrl = "0"
   Property  72: icm20602_proxy.icm20602.pwr_mgmt_1 = "1"
   Property  73: icm20602_proxy.icm20602.pwr_mgmt_2 = "0"
   Property  75: icm20602_proxy.icm20602.i2c_if = "0"
   Property  77: icm20602_proxy.icm20602.fifo_counth = "0"
   Property  78: icm20602_proxy.icm20602.fifo_countl = "0"
   Property  79: icm20602_proxy.icm20602.fifo_r_w = "127"
   Property  80: icm20602_proxy.icm20602.who_am_i = "18"
   Property  82: icm20602_proxy.icm20602.xa_offset_h = "2"
   Property  83: icm20602_proxy.icm20602.xa_offset_l = "184"
   Property  85: icm20602_proxy.icm20602.ya_offset_h = "253"
   Property  86: icm20602_proxy.icm20602.ya_offset_l = "22"
   Property  88: icm20602_proxy.icm20602.za_offset_h = "252"
   Property  89: icm20602_proxy.icm20602.za_offset_l = "238"
   WARNING: Continuous Mode will not work properly if the device is set up for server mode!WARNING: Continuous Mode will not work properly if the device is set up for server mode!

Example of stdout for ``continuous_data`` mode
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block::

   % ocpirun -v -d  continuous_data_mode.xml
   Available containers are:  0: PL:0 [model: hdl os:  platform: z3u], 1: rcc0 [model: rcc os: linux platform: xilinx18_3_aarch64]
   Actual deployment is:
   Instance  0 icm20602_proxy (spec ocpi.osp.epiq_solutions.devices.icm20602_proxy) on rcc container 1: rcc0, using icm20602_proxy in ../../artifacts//ocpi.osp.epiq_solutions.devices.icm20602_proxy.rcc.0.xilinx18_3_aarch64.so dated Mon Jan 10 20:40:42 2022
   Instance  1 icm20602_proxy.icm20602 (spec ocpi.osp.epiq_solutions.devices.icm20602) on hdl container 0: PL:0, using icm20602/c/icm20602 in ../../artifacts//ocpi.osp.epiq_solutions.empty_z3u_base_cnt_z3u_i2c_bus0.hdl.0.z3u.bitz dated Mon Jan 10 14:42:56 2022
   Application XML parsed and deployments (containers and artifacts) chosen [1 s 275 ms]
   Application established: containers, workers, connections all created [0 s 9 ms]
   Dump of all initial property values:
   Property   0: icm20602_proxy.init_config = "true"
   WARNING: Continuous Mode will not work properly if the device is set up for server mode!Press ctrl + c to cancel
   Info: Gyroscope: x=0    y=0    z=0    Info: Accelerometer: x=0  y=0  z=1
