.. timegate_csts_test_app application

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _timegate_csts_test_app:

app: ``timegate_csts_test_app``
==========================

Description
-----------

.. warning::

   This test app was copied from another FOSS OSPs (E3xx, PlutoSDR) and modified to
   support the csts-protocol in support of the Z3u.

   THIS APP IS PROVIDED AS A STARTING POINT AND NOT MANDATORY FOR THE ACCEPTANCE
   OF THE Z3u OSP.

   THIS APP EXECUTES "SUCCESSFULLY" BUT REQUIRES ADDITIONAL TOOLING TO BE PROVIDED
   BY FOSS. IT REQUIRES THE GENERATION OF A TEST INPUT FILE WHICH INCLUDES THE
   CORRECT MESSAGING FOR TIMED TRANMIT.

Reads from a file which has messaging information that controls the timed-transmit
of a tone.

The DRC used by this implementation does not include dynamic configuration of the
RF filter path, but rather the OAS contains a fix configuration in support of
RF frequency 1.8 GHz and sample rate of 10 MSps.

Execution of this app uses a convenient feature of OpenCPI with respects to selecting
the target bitstream. Specifically, the "timestamper_scdcd_csts_timegate_csts_asm" is
used for this test app even though it include several more workers than what is required
of the OAS.

Prerequisites
-------------

The ``z3u`` has been properly installed and deployed, and the application assembly
bitstream is built. Specifically, this test application requires that the following
assets are built:

  * HDL Assembly: ``timestamper_scdcd_csts_timegate_csts_asm`` (from the Z3u OSP)
  * DRC Worker: ``drc_z3u_mode_2_cmos_timegate_csts.rcc`` (from the Z3u OSP)

Execution
---------

.. note::
   The instructions are written for **Network Mode**

#. Boot the Matchstiq Z3U, setup for Network Mode, then browse to the following directory:

   % ``cd /mnt/ocpi_z3u/applications/timegate_csts_test_app/``

#. Setup the artifact search path:

   % ``export OCPI_LIBRARY_PATH=../../hdl/assemblies/timestamper_scdcd_csts_timegate_csts_asm:../../hdl/devices/drc_z3u_mode_2_cmos_timegate_csts.rcc:/mnt/ocpi_platform/artifacts:/mnt/ocpi_core/artifacts:/mnt/ocpi_assets/artifacts``

#. Setup a Spectrum Analyzer to center freq=1.8 GHz and span=100MHz. Connect an RF coax
   cable from the Z3u/TRx to the SpecA.

#. Run the application

   % ``./scripts/run_test_app.sh``

#. Verify output on SpecA:

   #. Post-processing of the transmitted data is not yet supported. However:

      #. It is expected that with a proper input file, the SpecA would show a transmitted
	 tone toggling on/off with a know rate.

Example of stdout
~~~~~~~~~~~~~~~~~

.. code-block::

   % export OCPI_LIBRARY_PATH=../../hdl/assemblies/timestamper_scdcd_csts_timegate_csts_asm:../../hdl/devices/drc_z3u_mode_2_cmos_timegate_csts.rcc:/mnt/ocpi_platform/artifacts:/mnt/ocpi_core/artifacts:/mnt/ocpi_assets/artifacts
   % ./scripts/run_test_app.sh
   Setting the time_now property to '0' on instance 'p/time_server'
   Available containers are:  0: PL:0 [model: hdl os:  platform: z3u], 1: rcc0 [model: rcc os: linux platform: xilinx18_3_aarch64]
   Actual deployment is:
   Instance  0 pcal65240 (spec ocpi.osp.epiq_solutions.devices.pcal6524) on hdl container 0: PL:0, using pcal6524/c/pcal65240 in ../../hdl/assemblies/timestamper_scdcd_csts_timegate_csts_asm/container-timestamper_scdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/timestamper_scdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:14:13 2022
   Instance  1 pcal65241 (spec ocpi.osp.epiq_solutions.devices.pcal6524) on hdl container 0: PL:0, using pcal6524/c/pcal65241 in ../../hdl/assemblies/timestamper_scdcd_csts_timegate_csts_asm/container-timestamper_scdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/timestamper_scdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:14:13 2022
   Instance  2 pcal65242 (spec ocpi.osp.epiq_solutions.devices.pcal6524) on hdl container 0: PL:0, using pcal6524/c/pcal65242 in ../../hdl/assemblies/timestamper_scdcd_csts_timegate_csts_asm/container-timestamper_scdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/timestamper_scdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:14:13 2022
   Instance  3 file_read (spec ocpi.core.file_read) on rcc container 1: rcc0, using file_read in /mnt/ocpi_core/artifacts/ocpi.core.file_read.rcc.0.xilinx18_3_aarch64.so dated Tue Nov 30 23:08:09 2021
   Instance  4 drc (spec ocpi.platform.drc) on rcc container 1: rcc0, using drc_z3u_mode_2_cmos_timegate_csts in ../../hdl/devices/drc_z3u_mode_2_cmos_timegate_csts.rcc/target-xilinx18_3_aarch64/drc_z3u_mode_2_cmos_timegate_csts.so dated Wed Jan 12 20:48:27 2022
   Instance  5 drc.config (spec ocpi.platform.devices.platform_ad9361_config_csts) on hdl container 0: PL:0, using platform_ad9361_config_csts/c/platform_ad9361_config_csts in ../../hdl/assemblies/timestamper_scdcd_csts_timegate_csts_asm/container-timestamper_scdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/timestamper_scdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:14:13 2022
   Instance  6 drc.data_sub (spec ocpi.platform.devices.platform_csts_ad9361_data_sub) on hdl container 0: PL:0, using platform_csts_ad9361_data_sub-3/c/platform_csts_ad9361_data_sub in ../../hdl/assemblies/timestamper_scdcd_csts_timegate_csts_asm/container-timestamper_scdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/timestamper_scdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:14:13 2022
   Instance  7 drc.rx_qadc0 (spec ocpi.platform.devices.data_src_qadc_csts) on hdl container 0: PL:0, using timegate_csts/a/timegate_csts in ../../hdl/assemblies/timestamper_scdcd_csts_timegate_csts_asm/container-timestamper_scdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/timestamper_scdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:14:13 2022
   Instance  8 drc.rx_csts_to_iqstream0 (spec ocpi.assets.misc_comps.csts_to_iqstream) on hdl container 0: PL:0, using data_sink_qdac_csts/c/data_sink_qdac_csts in ../../hdl/assemblies/timestamper_scdcd_csts_timegate_csts_asm/container-timestamper_scdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/timestamper_scdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:14:13 2022
   Instance  9 drc.rx_complex_mixer0 (spec ocpi.assets.dsp_comps.complex_mixer) on hdl container 0: PL:0, using platform_ad9361_spi_csts/c/platform_ad9361_spi_csts in ../../hdl/assemblies/timestamper_scdcd_csts_timegate_csts_asm/container-timestamper_scdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/timestamper_scdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:14:13 2022
   Instance 10 drc.rx_cic_dec0 (spec ocpi.assets.dsp_comps.cic_dec) on hdl container 0: PL:0, using data_sink_qdac_csts_ad9361_sub-1/c/data_sink_qdac_csts_ad9361_sub in ../../hdl/assemblies/timestamper_scdcd_csts_timegate_csts_asm/container-timestamper_scdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/timestamper_scdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:14:13 2022
   Application XML parsed and deployments (containers and artifacts) chosen [13 s 985 ms]
   Application established: containers, workers, connections all created [0 s 154 ms]
   ad9361_init : AD936x Rev 2 successfully initialized
   Application started/running [0 s 641 ms]
   Waiting for up to 5 seconds for application to finish
   Application is now considered finished after waiting 5 seconds [5 s 9 ms]
   FDD:0Lvl:0Pin:0  Instance p/time_server of io worker time_server (spec ocpi.core.devices.time_server) with index 1
   0           ocpi_debug: false
   1          ocpi_endian: little
   2         ocpi_version: 0x0
   3             time_now: 0x154bd1c427
   4                delta: 0x0
   5               PPS_ok: false
   6 enable_time_now_updates_from_PPS: <unreadable>
   7 valid_requires_write_to_time_now: <unreadable>
   8            frequency: 100000000
   9    PPS_tolerance_PPM: 0x3e8
   10             PPS_test: false
   11 clr_status_sticky_bits: <unreadable>
   12 force_time_now_to_free_running: <unreadable>
   13       PPS_out_source: <unreadable>
   14 force_time_now_valid: <unreadable>
   15 force_time_now_invalid: <unreadable>
   16 PPS_lost_sticky_error: false
   17 time_now_updated_by_PPS_sticky: false
   18  time_now_set_sticky: true
   19 PPS_lost_last_second_error: false
   20            PPS_count: 0x1
   21     ticks_per_second: 0x0
   22            using_PPS: true
   23           time_valid: true
