.. timestamper_scdcd_csts_test_app application

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _timestamper_scdcd_csts_test_app:

app: ``timestamper_scdcd_csts_test_app``
========================================

Description
-----------

.. warning::

   This test app was copied from another FOSS OSPs (E3xx, PlutoSDR) and modified to
   support the csts-protocol in support of the Z3u.

   THIS APP IS PROVIDED AS A STARTING POINT AND NOT MANDATORY FOR THE ACCEPTANCE
   OF THE Z3u OSP.

   THIS APP EXECUTES "SUCCESSFULLY" BUT REQUIRES ADDITIONAL TOOLING TO BE PROVIDED
   BY FOSS, IN ORDER TO VALIDATE THE CAPTURED DATA.

..

Captures samples from an ADC Device Worker that implements the csts-protocol.

As can be determined via the examination of the OAS, a DRC worker is NOT used
by this application to configuration of the RF filter path is fixed at an RF
center frequency=1.8 GHz. Instead, the PCAL device workers are explicitly
instanced with property values to configure for the desired center frequency,
etc. This is done to prove to OSP developers that DRC workers are not
mandatory for the development of an OSP in its early stages.

Per the review of scripts/run_test_app.sh, there are several "modes" with which
the test application can be executed:

* 0 AD9361 BIST tone generation on RX port @ fs*(0+1)/32
* 1 AD9361 BIST tone generation on RX port @ fs*(1+1)/32
* 2 AD9361 BIST tone generation on RX port @ fs*(2+1)/32
* 3 AD9361 BIST tone generation on RX port @ fs*(3+1)/32
* 4 AD9361 BIST DAC to ADC Loopback
* 5 External Signal Generator

Verification of the output is yet to be created.

Prerequisites
-------------

The ``z3u`` has been properly installed and deployed, and the application assembly
bitstream is built. Specifically, this test application requires that the following
assets are built:

  * HDL Assembly: ``timestamper_scdcd_csts_asm`` (from the Z3u OSP)

Execution
---------

.. note::

   The instructions are written for **Network Mode**

#. Boot the Matchstiq Z3U, setup for Network Mode, then browse to the following directory:

   ``% cd /applications/timestamper_scdcd_csts_test_app/``

#. Setup the artifact search path:

   ::

      % export OCPI_LIBRARY_PATH=../../hdl/assemblies/timestamper_scdcd_csts_asm:/mnt/ocpi_platform/artifacts:/mnt/ocpi_core/artifacts:/mnt/ocpi_assets/artifacts

   ..

#. Review comments within the run_test_app.sh to determine the desired test mode

   - (OPTIONAL) mode #5 requires the setup of an external signal generator

#. Run the application

   #. Execute script with desire arguments:

      For example: ``./scripts/run_test_app.sh <mode> <sample rate> <duration>``

      ``% ./scripts/run_test_app.sh 2 3000000 1``

      .. warning::

         The app does not always gracefully finish and sometimes requires the user to execute "Ctrl-C"
         for termination

      ..

#. Verify output:

   #. Post-processing of the captured data is not yet supported. However:

      #. Captured data should show the expected time samples field increment in a
	 consistent, predetermined manner.

Example of stdout
^^^^^^^^^^^^^^^^^

.. code-block::

   % export OCPI_LIBRARY_PATH=../../hdl/assemblies/timestamper_scdcd_csts_asm:/mnt/ocpi_platform/artifacts:/mnt/ocpi_core/artifacts:/mnt/ocpi_assets/artifacts
   % ./scripts/run_test_app.sh 2 3000000 1
   Expected tone at 281250
   Available containers are:  0: PL:0 [model: hdl os:  platform: z3u], 1: rcc0 [model: rcc os: linux platform: xilinx18_3_aarch64]
   Actual deployment is:
   Instance  0 pcal65240 (spec ocpi.osp.epiq_solutions.devices.pcal6524) on hdl container 0: PL:0, using pcal6524/c/pcal65240 in ../../hdl/assemblies/timestamper_scdcd_csts_asm/container-timestamper_scdcd_csts_asm_z3u_base_cnt_1rx_0tx_mode_2_cmos_csts/target-zynq_ultra/timestamper_scdcd_csts_asm_z3u_base_cnt_1rx_0tx_mode_2_cmos_csts.bitz dated Thu Jan 13 13:37:38 2022
   Instance  1 pcal65241 (spec ocpi.osp.epiq_solutions.devices.pcal6524) on hdl container 0: PL:0, using pcal6524/c/pcal65241 in ../../hdl/assemblies/timestamper_scdcd_csts_asm/container-timestamper_scdcd_csts_asm_z3u_base_cnt_1rx_0tx_mode_2_cmos_csts/target-zynq_ultra/timestamper_scdcd_csts_asm_z3u_base_cnt_1rx_0tx_mode_2_cmos_csts.bitz dated Thu Jan 13 13:37:38 2022
   Instance  2 pcal65242 (spec ocpi.osp.epiq_solutions.devices.pcal6524) on hdl container 0: PL:0, using pcal6524/c/pcal65242 in ../../hdl/assemblies/timestamper_scdcd_csts_asm/container-timestamper_scdcd_csts_asm_z3u_base_cnt_1rx_0tx_mode_2_cmos_csts/target-zynq_ultra/timestamper_scdcd_csts_asm_z3u_base_cnt_1rx_0tx_mode_2_cmos_csts.bitz dated Thu Jan 13 13:37:38 2022
   Instance  3 timestamper_scdcd_csts (spec ocpi.assets.util_comps.timestamper_scdcd_csts) on hdl container 0: PL:0, using timestamper_scdcd_csts/a/timestamper_scdcd_csts in ../../hdl/assemblies/timestamper_scdcd_csts_asm/container-timestamper_scdcd_csts_asm_z3u_base_cnt_1rx_0tx_mode_2_cmos_csts/target-zynq_ultra/timestamper_scdcd_csts_asm_z3u_base_cnt_1rx_0tx_mode_2_cmos_csts.bitz dated Thu Jan 13 13:37:38 2022
   Instance  4 file_write (spec ocpi.core.file_write) on rcc container 1: rcc0, using file_write in /mnt/ocpi_core/artifacts/ocpi.core.file_write.rcc.0.xilinx18_3_aarch64.so dated Tue Nov 30 23:08:36 2021
   Instance  5 platform_ad9361_spi_csts (spec ocpi.platform.devices.platform_ad9361_spi_csts) on hdl container 0: PL:0, using platform_ad9361_spi_csts/c/platform_ad9361_spi_csts in ../../hdl/assemblies/timestamper_scdcd_csts_asm/container-timestamper_scdcd_csts_asm_z3u_base_cnt_1rx_0tx_mode_2_cmos_csts/target-zynq_ultra/timestamper_scdcd_csts_asm_z3u_base_cnt_1rx_0tx_mode_2_cmos_csts.bitz dated Thu Jan 13 13:37:38 2022
   Instance  6 platform_ad9361_config_csts (spec ocpi.platform.devices.platform_ad9361_config_csts) on hdl container 0: PL:0, using platform_ad9361_config_csts/c/platform_ad9361_config_csts in ../../hdl/assemblies/timestamper_scdcd_csts_asm/container-timestamper_scdcd_csts_asm_z3u_base_cnt_1rx_0tx_mode_2_cmos_csts/target-zynq_ultra/timestamper_scdcd_csts_asm_z3u_base_cnt_1rx_0tx_mode_2_cmos_csts.bitz dated Thu Jan 13 13:37:38 2022
   Instance  7 ad9361_config_proxy (spec ocpi.platform.devices.ad9361_config_proxy) on rcc container 1: rcc0, using platform_ad9361_config_proxy-1 in /mnt/ocpi_platform/artifacts/ocpi.platform.devices.platform_ad9361_config_proxy.rcc.1.xilinx18_3_aarch64.so dated Thu Dec 16 21:04:11 2021
   Instance  8 data_src_qadc_csts (spec ocpi.platform.devices.data_src_qadc_csts) on hdl container 0: PL:0, using data_src_qadc_csts-1/c/data_src_qadc_csts0 in ../../hdl/assemblies/timestamper_scdcd_csts_asm/container-timestamper_scdcd_csts_asm_z3u_base_cnt_1rx_0tx_mode_2_cmos_csts/target-zynq_ultra/timestamper_scdcd_csts_asm_z3u_base_cnt_1rx_0tx_mode_2_cmos_csts.bitz dated Thu Jan 13 13:37:38 2022
   Application XML parsed and deployments (containers and artifacts) chosen [13 s 446 ms]
   ad9361_init : AD936x Rev 2 successfully initialized
   Application established: containers, workers, connections all created [1 s 461 ms]
   Application started/running [0 s 11 ms]
   [1 s 5 ms]

..
