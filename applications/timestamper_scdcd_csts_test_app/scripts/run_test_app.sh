#!/bin/sh

# NOTICE!!!!: 
# Using ocpirun -p <prop> to overwrite a RAW Properties DOES NOT seem to be working!
# The syntax remains as a reminder of how much easier things would be if it did work.
# So, as an alternative implementation, the 'sed' command was used to modify the OAS XML on-the-fly.

# Steps for executing this script:
# 1 - Setup remote device for Standalone or Network Mode
# 2 - Setup OCPI_LIBRARY_PATH: export OCPI_LIBRARY_PATH=../artifacts:/mnt/ocpi_core/artifacts:/mnt/ocpi_platforms/artifacts:/mnt/ocpi_assets/artifacts
# 3 - Setup External Signal Generator (OPTIONAL mode #x)
# 4 - Execute script with desire arguments: ./run_test_app.sh <mode> <sample rate> <duration>
# 5 - Verify output (on HOST machine):

if [ -z "$3" ]; then
    echo "Usage is: $0 <mode> <sample rate> <duration>"
    exit 1
fi

# Description of arguments:
# <mode> 
# 0 AD9361 BIST tone generation on RX port @ fs*(0+1)/32
# 1 AD9361 BIST tone generation on RX port @ fs*(1+1)/32
# 2 AD9361 BIST tone generation on RX port @ fs*(2+1)/32
# 3 AD9361 BIST tone generation on RX port @ fs*(3+1)/32
# 4 AD9361 BIST DAC to ADC Loopback
# 5 External Signal Generator
#
# <sample rate> 
# Must match that which is timestamper_scdcd_csts_test_app.xml, i.e. (YES, this is manual step)
#  <instance component='ocpi.osp.epiq_solutions.devices.ad9361_config_proxy'>
#  <property name='rx_sampling_freq' value='3000000'/>
#  <property name='tx_sampling_freq' value='3000000'/>
#
# <duration>  units seconds. 1 sec is typically enough time

case $1 in
    0) # AD9361 BIST tone generation on RX port @ fs*(0+1)/32
#	PROP_BIST="-pz3u_ad9361_spi_wsi=test_bist_config=0xb"
#	sed 's/<property name='bist_tone' value='mode 2, freq_Hz 0, level_dB 0, mask 0'/>/<property name='bist_tone' value='mode 2, freq_Hz 0, level_dB 0, mask 0'/>/g' timestamper_scdcd_csts_test_app.xml > tmp_app.xml
	sed "s/bist_loopback' value='?'/bist_loopback' value='0'/g" timestamper_scdcd_csts_test_app.xml > tmp_app.xml
	echo "Expected tone at" $(($2*($1+1)/32))
    ;;
    1) # AD9361 BIST tone generation on RX port @ fs*(1+1)/32
	#PROP_BIST="-pz3u_ad9361_spi_wsi=test_bist_config=0x4b"
	sed "s/bist_loopback' value='?'/bist_loopback' value='0'/g" timestamper_scdcd_csts_test_app.xml > tmp_app.xml
	sed -i 's/freq_Hz 0/freq_Hz 1/g' tmp_app.xml
	echo "Expected tone at" $(($2*($1+1)/32))
    ;;
    2) # AD9361 BIST tone generation on RX port @ fs*(2+1)/32
	#PROP_BIST="-pz3u_ad9361_spi_wsi=test_bist_config=0x8b"
	sed "s/bist_loopback' value='?'/bist_loopback' value='0'/g" timestamper_scdcd_csts_test_app.xml > tmp_app.xml
	sed -i 's/freq_Hz 0/freq_Hz 2/g' tmp_app.xml
	echo "Expected tone at" $(($2*($1+1)/32))
    ;;
    3) # AD9361 BIST: tone generation on RX port @ fs*(3+1)/32
	#PROP_BIST="-pz3u_ad9361_spi_wsi=test_bist_config=0xcb"
	sed "s/bist_loopback' value='?'/bist_loopback' value='0'/g" timestamper_scdcd_csts_test_app.xml > tmp_app.xml
	sed -i 's/freq_Hz 0/freq_Hz 3/g' tmp_app.xml
	echo "Expected tone at" $(($2*($1+1)/32))
    ;;
    4) # AD9361 BIST DAC to ADC Loopback
	#PROP_BIST="-pz3u_ad9361_spi_wsi=test_observe_config=0x1"
	sed "s/bist_loopback' value='?'/bist_loopback' value='1'/g" timestamper_scdcd_csts_test_app.xml > tmp_app.xml
	sed -i "s/dac_adc_data_select' value='0x00'/dac_adc_data_select' value='0x42'/g" tmp_app.xml
	#sed -i "s/<property name='bist_tone' value='mode 2, freq_Hz 0, level_dB 0, mask 0'/>/<!--property name='bist_tone' value='mode 2, freq_Hz 0, level_dB 0, mask 0'/-->/g" tmp_app.xml
	sed -i "s/mode 2, freq_Hz/mode 0, freq_Hz/g" tmp_app.xml
	echo "Expected tone at ~2*frequency_control_word/2**(16)*100MHz=312500"
    ;;
    5) # External Signal Generator
	#PROP_BIST="-pz3u_ad9361_spi_wsi=test_bist_config=0x00"
	sed "s/bist_loopback' value='?'/bist_loopback' value='0'/g" timestamper_scdcd_csts_test_app.xml > tmp_app.xml
	sed -i "s/dac_adc_data_select' value='0x00'/dac_adc_data_select' value='0x02'/g" tmp_app.xml
	sed -i "s/<property name='bist_tone' value='mode 2, freq_Hz 0, level_dB 0, mask 0'\/>//g" tmp_app.xml
	echo "Expected tone is determined by external signal generator. " \
	    "It must be within the AD9361's tune and BW and Z3U's input power range:" \
	    "tx/rx_log_freq=1.8GHz, BW=56 MHz, -35dBm."
    ;;
    *)
	echo "Not supported"
	;;
esac

# EXECUTE - Better to reset the time server, but not critical.
#ocpihdl wreset 1; ocpihdl wunreset 1; ocpirun -v -t 3 timestamper_scdcd_csts_test_app.xml
#ocpirun -v -t $3 $PROP_BIST timestamper_scdcd_csts_test_app.xml
ocpirun -v -x -t $3 tmp_app.xml

# VERIFY - MUST be performed on the Host machine
#TBD
