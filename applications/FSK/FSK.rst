.. FSK application

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _FSK:

app: ``FSK``
============

Description
-----------

This app transfers an image (prepended with a sync pattern) in/out of the FPGA, which
implements an assembly of digital signal processing modules that supports a simple
FSK modulator/demodulator, then performs additional processing on the received data by
the ARM in which to recover the original decoded image.

Hardware Portability
^^^^^^^^^^^^^^^^^^^^

In its current state, this application requires some light modifications to be ported to another
platform.

Prerequisites
-------------

The ``z3u`` has been properly installed and deployed, and the application assembly
bitstream is built. To name a few, this test application requires that the following
assets are built:

  * HDL Assembly: ``fsk_filerw`` (from the Z3u OSP)
  * RCC Worker: ``file_read.rcc`` (from ``core`` project)
  * RCC Worker: ``file_write.rcc`` (from ``core`` project)

Execution
---------

The execution commands herein are subject to change. This application can be run with
``ocpirun`` or via a cross-compiled ACI. Both options are detailed below.

.. note::

   The instructions are written for **Network Mode**
..

#. Boot the Matchstiq Z3U, setup for Network Mode, then browse to the following directory:

   ``% cd /mnt/ocpi_z3u/applications/FSK/``

#. Setup the artifact search path:

   ``% export OCPI_LIBRARY_PATH=../../hdl/assemblies/fsk_filerw:/mnt/ocpi_assets/artifacts:/mnt/ocpi_core/artifacts``

#. Run the application

   A. Using via ``ocpirun``

      #. Generate the FIR filter tap files

	 ``% make``

      #. Run

	 ``% ocpirun -v -d app_fsk_filerw.xml``

	 .. code-block::

	    % export OCPI_LIBRARY_PATH=../../hdl/assemblies/fsk_filerw:/mnt/ocpi_assets/artifacts:/mnt/ocpi_core/artifacts
	    % ocpirun -v -d app_fsk_filerw.xml
	    Available containers are:  0: PL:0 [model: hdl os:  platform: z3u], 1: rcc0 [model: rcc os: linux platform: xilinx18_3_aarch64]
	    Actual deployment is:
	    Instance  0 mfsk_mapper (spec ocpi.assets.comms_comps.mfsk_mapper) on hdl container 0: PL:0, using mfsk_mapper/a/mfsk_mapper in ../../hdl/assemblies/fsk_filerw/container-fsk_filerw_z3u_base/target-zynq_ultra/fsk_filerw_z3u_base.bitz dated Mon Jan 24 22:09:22 2022
	    Instance  1 zero_pad (spec ocpi.assets.util_comps.zero_pad) on hdl container 0: PL:0, using zero_pad-1/a/zero_pad in ../../hdl/assemblies/fsk_filerw/container-fsk_filerw_z3u_base/target-zynq_ultra/fsk_filerw_z3u_base.bitz dated Mon Jan 24 22:09:22 2022
	    Instance  2 tx_fir_real (spec ocpi.assets.dsp_comps.fir_real_sse) on hdl container 0: PL:0, using fir_real_sse/a/tx_fir_real in ../../hdl/assemblies/fsk_filerw/container-fsk_filerw_z3u_base/target-zynq_ultra/fsk_filerw_z3u_base.bitz dated Mon Jan 24 22:09:22 2022
	    Instance  3 phase_to_amp_cordic (spec ocpi.assets.dsp_comps.phase_to_amp_cordic) on hdl container 0: PL:0, using phase_to_amp_cordic-1/a/phase_to_amp_cordic in ../../hdl/assemblies/fsk_filerw/container-fsk_filerw_z3u_base/target-zynq_ultra/fsk_filerw_z3u_base.bitz dated Mon Jan 24 22:09:22 2022
	    Instance  4 cic_int (spec ocpi.assets.dsp_comps.cic_int) on hdl container 0: PL:0, using cic_int-5/a/cic_int in ../../hdl/assemblies/fsk_filerw/container-fsk_filerw_z3u_base/target-zynq_ultra/fsk_filerw_z3u_base.bitz dated Mon Jan 24 22:09:22 2022
	    Instance  5 complex_mixer (spec ocpi.assets.dsp_comps.complex_mixer) on hdl container 0: PL:0, using complex_mixer/a/complex_mixer in ../../hdl/assemblies/fsk_filerw/container-fsk_filerw_z3u_base/target-zynq_ultra/fsk_filerw_z3u_base.bitz dated Mon Jan 24 22:09:22 2022
	    Instance  6 cic_dec (spec ocpi.assets.dsp_comps.cic_dec) on hdl container 0: PL:0, using cic_dec-5/a/cic_dec in ../../hdl/assemblies/fsk_filerw/container-fsk_filerw_z3u_base/target-zynq_ultra/fsk_filerw_z3u_base.bitz dated Mon Jan 24 22:09:22 2022
	    Instance  7 rp_cordic (spec ocpi.assets.dsp_comps.rp_cordic) on hdl container 0: PL:0, using rp_cordic/a/rp_cordic in ../../hdl/assemblies/fsk_filerw/container-fsk_filerw_z3u_base/target-zynq_ultra/fsk_filerw_z3u_base.bitz dated Mon Jan 24 22:09:22 2022
	    Instance  8 rx_fir_real (spec ocpi.assets.dsp_comps.fir_real_sse) on hdl container 0: PL:0, using fir_real_sse/a/rx_fir_real in ../../hdl/assemblies/fsk_filerw/container-fsk_filerw_z3u_base/target-zynq_ultra/fsk_filerw_z3u_base.bitz dated Mon Jan 24 22:09:22 2022
	    Instance  9 file_read (spec ocpi.core.file_read) on rcc container 1: rcc0, using file_read in /mnt/ocpi_core/artifacts/ocpi.core.file_read.rcc.0.xilinx18_3_aarch64.so dated Mon Jan 24 20:07:14 2022
	    Instance 10 baudTracking (spec ocpi.assets.dsp_comps.baudTracking) on rcc container 1: rcc0, using Baudtracking_simple in /mnt/ocpi_assets/artifacts/ocpi.assets.dsp_comps.Baudtracking_simple.rcc.0.xilinx18_3_aarch64.so dated Mon Jan 24 20:11:17 2022
	    Instance 11 real_digitizer (spec ocpi.assets.dsp_comps.real_digitizer) on rcc container 1: rcc0, using real_digitizer in /mnt/ocpi_assets/artifacts/ocpi.assets.dsp_comps.real_digitizer.rcc.0.xilinx18_3_aarch64.so dated Mon Jan 24 20:11:13 2022
	    Instance 12 file_write (spec ocpi.core.file_write) on rcc container 1: rcc0, using file_write in /mnt/ocpi_core/artifacts/ocpi.core.file_write.rcc.0.xilinx18_3_aarch64.so dated Mon Jan 24 20:07:42 2022
	    Application XML parsed and deployments (containers and artifacts) chosen [10 s 508 ms]
	    Application established: containers, workers, connections all created [0 s 664 ms]
	    Application started/running [0 s 370 ms]
	    Waiting for application to finish (no time limit)
	    real_digitizer: sync pattern 0xFACE found
	    Application finished [0 s 935 ms]

         ..

   B. Using the ACI:

      #. Ensure the ACI is cross-compiled for the target RCC Platform

	 ``% ocpidev build --rcc-platform xilinx18_3_aarch64``

      #. Run the app

	 ``% ./target-xilinx18_3_aarch64/FSK filerw``

	 .. code-block::

	    % ./target-xilinx18_3_aarch64/FSK filerw
	    container is z3u
	    FSK APP for Z3U
	    model is hdl, container is z3u
	    Application properties are found in XML file: app_fsk_filerw.xml
	    App initialized.
	    App started.
	    Waiting for done signal from file_write.
	    real_digitizer: sync pattern 0xFACE found
	    App stopped.
	    Bytes to file : 8950
	    TX FIR Real Peak       = 4696
	    Phase to Amp Magnitude = 20000
	    RP Cordic Magnitude    = 19483
	    RX FIR Real Peak       = 13701
	    Application complete

         ..

#. Verification

   - Returning to the Development Host and viewing the output data file

     ``$ eog odata/out_app_fsk_filerw.bin``
