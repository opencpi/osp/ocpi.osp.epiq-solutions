.. z3u_i2c_bus2_test_app application

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _z3u_i2c_bus2_test_app:

app: ``z3u_i2c_bus2_test_app``
==========================

Description
-----------

Tests the access of the various I2C devices of the "bus2" of the Z3u. Generally, reading of the
device worker properties is performed, but writing of some properties may also be performed.

This document assumes that the user has reviewed the datasheets for the RCC and HDL workers that
are present in this application (sit5356, tmp103).

**sit5356**: This component in the application will present the default or user-input property
values of the Digital Pull Range Control (input) and PPM Shift from Nominal (input) values, and
depending on those values will present the property values of the Control Word Value (output) and
Shifted Frequency (output) values.

**tmp103**: This component in the application will present the temperature property value that is
present at the temp_reg register in degrees Celsius.

Hardware Portability
-------------------

The I2C devices workers implemented on this bus are not unique to the Z3u and it is possible
to reuse on other platforms. However, the subdevice worker use here is not likely portable,
therefore a new one for the target platform will need to be created.

Prerequisites
-------------

The ``z3u`` has been properly installed and deployed, and the application assembly
bitstream is built. Specifically, this test application requires that the following
assets are built:

  * HDL Assembly: ``empty`` with container ``cnt_z3u_i2c_bus2.xml`` (from the Z3u OSP)
  * RCC Proxy: ``sit5356_proxy.rcc`` (from the Z3u OSP)
  * RCC Proxy: ``tmp103_proxy.rcc`` (from the Z3u OSP)

Execution
---------

.. note::

   The instructions are written for **Network Mode**

#. Boot the Matchstiq Z3U, setup for Network Mode, then browse to the following directory:

   ``% cd /mnt/ocpi_z3u/applications/z3u_i2c_bus2_test_app/``

#. Setup the artifact search path:

   ``% export OCPI_LIBRARY_PATH=../../artifacts``

#. Run the application

   ``% ocpirun -v -d -t 1 z3u_i2c_bus2_test_app.xml``

Example of stdout, including the byte-swap information
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

   Available containers are:  0: PL:0 [model: hdl os:  platform: z3u], 1: rcc0 [model: rcc os: linux platform: xilinx18_3_aarch64]
   Actual deployment is:
   Instance  0 sit5356_proxy (spec ocpi.osp.epiq_solutions.devices.sit5356_proxy) on rcc container 1: rcc0, using sit5356_proxy-1 in ../../artifacts/ocpi.osp.epiq_solutions.devices.sit5356_proxy.rcc.1.xilinx18_3_aarch64.so dated Tue Dec  7 17:32:29 2021
   Instance  1 tmp103_fpga (spec ocpi.osp.epiq_solutions.devices.tmp103_proxy) on rcc container 1: rcc0, using tmp103_proxy in ../../artifacts/ocpi.osp.epiq_solutions.devices.tmp103_proxy.rcc.0.xilinx18_3_aarch64.so dated Tue Dec  7 17:32:21 2021
   Instance  2 tmp103_rfic (spec ocpi.osp.epiq_solutions.devices.tmp103_proxy) on rcc container 1: rcc0, using tmp103_proxy in ../../artifacts/ocpi.osp.epiq_solutions.devices.tmp103_proxy.rcc.0.xilinx18_3_aarch64.so dated Tue Dec  7 17:32:21 2021
   Instance  3 tmp103_ocs (spec ocpi.osp.epiq_solutions.devices.tmp103_proxy) on rcc container 1: rcc0, using tmp103_proxy in ../../artifacts/ocpi.osp.epiq_solutions.devices.tmp103_proxy.rcc.0.xilinx18_3_aarch64.so dated Tue Dec  7 17:32:21 2021
   Instance  4 sit5356_proxy.sit5356 (spec ocpi.osp.epiq_solutions.devices.sit5356) on hdl container 0: PL:0, using sit5356/c/sit5356 in ../../artifacts/ocpi.osp.epiq_solutions.empty_z3u_base_cnt_z3u_i2c_bus2.hdl.0.z3u.bitz dated Fri Jan  7 20:30:43 2022
   Instance  5 tmp103_fpga.tmp103 (spec ocpi.osp.epiq_solutions.devices.tmp103) on hdl container 0: PL:0, using tmp103/c/tmp1030 in ../../artifacts/ocpi.osp.epiq_solutions.empty_z3u_base_cnt_z3u_i2c_bus2.hdl.0.z3u.bitz dated Fri Jan  7 20:30:43 2022
   Instance  6 tmp103_rfic.tmp103 (spec ocpi.osp.epiq_solutions.devices.tmp103) on hdl container 0: PL:0, using tmp103/c/tmp1031 in ../../artifacts/ocpi.osp.epiq_solutions.empty_z3u_base_cnt_z3u_i2c_bus2.hdl.0.z3u.bitz dated Fri Jan  7 20:30:43 2022
   Instance  7 tmp103_ocs.tmp103 (spec ocpi.osp.epiq_solutions.devices.tmp103) on hdl container 0: PL:0, using tmp103/c/tmp1032 in ../../artifacts/ocpi.osp.epiq_solutions.empty_z3u_base_cnt_z3u_i2c_bus2.hdl.0.z3u.bitz dated Fri Jan  7 20:30:43 2022
   Application XML parsed and deployments (containers and artifacts) chosen [4 s 464 ms]
   Application established: containers, workers, connections all created [0 s 25 ms]
   Dump of all initial property values:
   Property   0: sit5356_proxy.dig_pull_range_ctrl = "6.25"
   Property   1: sit5356_proxy.ppm_shift_from_nominal = "0"
   Property   2: sit5356_proxy.ctrl_word_value = "0"
   Property   3: sit5356_proxy.shifted_frequency = "40000000"
   Property   7: tmp103_fpga.temperature = "35"
   Property   8: tmp103_fpga.temp_high_threshold = "60"
   Property   9: tmp103_fpga.temp_low_threshold = "-10"
   Property  10: tmp103_fpga.config = "2"
   Property  14: tmp103_rfic.temperature = "35"
   Property  15: tmp103_rfic.temp_high_threshold = "60"
   Property  16: tmp103_rfic.temp_low_threshold = "-10"
   Property  17: tmp103_rfic.config = "2"
   Property  21: tmp103_ocs.temperature = "34"
   Property  22: tmp103_ocs.temp_high_threshold = "60"
   Property  23: tmp103_ocs.temp_low_threshold = "-10"
   Property  24: tmp103_ocs.config = "2"
   Property  28: sit5356_proxy.sit5356.dig_freq_ctrl_lsw_reg = "0"
   Property  29: sit5356_proxy.sit5356.dig_freq_ctrl_msw_reg = "0"
   Property  30: sit5356_proxy.sit5356.dig_pull_range_ctrl_reg = "0"
   Property  34: sit5356_proxy.sit5356.frequency = "40000000" (parameter)
   Property  35: tmp103_fpga.tmp103.temp_reg = "35"
   Property  36: tmp103_fpga.tmp103.config_reg = "2"
   Property  37: tmp103_fpga.tmp103.tlow_reg = "246"
   Property  38: tmp103_fpga.tmp103.thigh_reg = "60"
   Property  42: tmp103_rfic.tmp103.temp_reg = "35"
   Property  43: tmp103_rfic.tmp103.config_reg = "2"
   Property  44: tmp103_rfic.tmp103.tlow_reg = "246"
   Property  45: tmp103_rfic.tmp103.thigh_reg = "60"
   Property  49: tmp103_ocs.tmp103.temp_reg = "34"
   Property  50: tmp103_ocs.tmp103.config_reg = "2"
   Property  51: tmp103_ocs.tmp103.tlow_reg = "246"
   Property  52: tmp103_ocs.tmp103.thigh_reg = "60"
   Application started/running [0 s 78 ms]
   [1 s 5 ms]
   Dump of all final property values:
   Property   0: sit5356_proxy.dig_pull_range_ctrl = "6.25"
   Property   1: sit5356_proxy.ppm_shift_from_nominal = "0"
   Property   2: sit5356_proxy.ctrl_word_value = "0"
   Property   3: sit5356_proxy.shifted_frequency = "40000000"
   Property   7: tmp103_fpga.temperature = "35"
   Property   8: tmp103_fpga.temp_high_threshold = "60"
   Property   9: tmp103_fpga.temp_low_threshold = "-10"
   Property  10: tmp103_fpga.config = "2"
   Property  14: tmp103_rfic.temperature = "35"
   Property  15: tmp103_rfic.temp_high_threshold = "60"
   Property  16: tmp103_rfic.temp_low_threshold = "-10"
   Property  17: tmp103_rfic.config = "2"
   Property  21: tmp103_ocs.temperature = "34"
   Property  22: tmp103_ocs.temp_high_threshold = "60"
   Property  23: tmp103_ocs.temp_low_threshold = "-10"
   Property  24: tmp103_ocs.config = "2"
   Property  28: sit5356_proxy.sit5356.dig_freq_ctrl_lsw_reg = "0"
   Property  29: sit5356_proxy.sit5356.dig_freq_ctrl_msw_reg = "0"
   Property  30: sit5356_proxy.sit5356.dig_pull_range_ctrl_reg = "0"
   Property  35: tmp103_fpga.tmp103.temp_reg = "35"
   Property  36: tmp103_fpga.tmp103.config_reg = "2"
   Property  37: tmp103_fpga.tmp103.tlow_reg = "246"
   Property  38: tmp103_fpga.tmp103.thigh_reg = "60"
   Property  42: tmp103_rfic.tmp103.temp_reg = "35"
   Property  43: tmp103_rfic.tmp103.config_reg = "2"
   Property  44: tmp103_rfic.tmp103.tlow_reg = "246"
   Property  45: tmp103_rfic.tmp103.thigh_reg = "60"
   Property  49: tmp103_ocs.tmp103.temp_reg = "34"
   Property  50: tmp103_ocs.tmp103.config_reg = "2"
   Property  51: tmp103_ocs.tmp103.tlow_reg = "246"
   Property  52: tmp103_ocs.tmp103.thigh_reg = "60"
   Byte Swapped dig_pull_range_ctrl_reg decimal: 0
   Byte Swapped dig_pull_range_ctrl_reg hex: 0x0000
   Byte Swapped dig_freq_ctrl_lsw_reg decimal: 0
   Byte Swapped dig_freq_ctrl_lsw_reg hex: 0x0000
   Byte Swapped dig_freq_ctrl_msw_reg decimal: 0
   Byte Swapped dig_freq_ctrl_msw_reg hex: 0x0000
   Byte Swapped dig_pull_range_ctrl_reg decimal: 0
   Byte Swapped dig_pull_range_ctrl_reg hex: 0x0000
   Byte Swapped dig_freq_ctrl_lsw_reg decimal: 0
   Byte Swapped dig_freq_ctrl_lsw_reg hex: 0x0000
   Byte Swapped dig_freq_ctrl_msw_reg decimal: 0
   Byte Swapped dig_freq_ctrl_msw_reg hex: 0x0000

..
