This is the source distribution of the OpenCPI System Package(s) (OSP) for
Epiq Solutions devices.

OpenCPI is Open Source Software, licensed with the LGPL3. See LICENSE.txt.

Build/View documentation
---
$ cd ocpi.osp.epiq_solutions/

$ ocpidoc build

$ firefox gen/doc/index.html

OpenCPI Z3u Getting Started Guide
---
See hdl/platform/z3u/doc/OpenCPI_Z3u_Getting_Started_Guide.rst

Guide for developing the Epiq Solutions Matchstiq Z3u OSP
---
See hdl/platform/z3u/doc/OpenCPI_Z3u_OSP_Developers_Guide.rst

Guide for developing Device / Proxy Workers for an I2C bus
---
See guide/matchstiq_z3u/OpenCPI_Device_Worker_Proxy_Developers_Guide.rst

Version Tested against: v2.1.0 and v2.2.0
---
Test results are captured in the hdl/platform/z3u/doc/OpenCPI_Z3u_OSP_Developers_Guide.rst

Notes:
---
- applications and hdl/assemblies
  - Contains copies of apps and assemblies from the OpenCPI assets project,
    with the addition of Matchstiq Z3U specific container XML and XDC files.
  - A desirable framework improvement would be to mitigate the need to copy
    assemblies from external projects in order to use containers made possible
    internally.
