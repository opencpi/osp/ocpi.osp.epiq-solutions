# [v2.4.7](https://gitlab.com/opencpi/osp/ocpi.osp.epiq-solutions/-/compare/v2.4.6...v2.4.7) (2024-01-10)

No Changes/additions since [OpenCPI Release v2.4.6](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.6)

# [v2.4.6](https://gitlab.com/opencpi/osp/ocpi.osp.epiq-solutions/-/compare/v2.4.5...v2.4.6) (2023-03-29)

No Changes/additions since [OpenCPI Release v2.4.5](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.5)

# [v2.4.5](https://gitlab.com/opencpi/osp/ocpi.osp.n3xx/-/compare/3641d85e...v2.4.5) (2023-03-16)

Initial release of Epiq Solutions (z3u) OSP for OpenCPI
