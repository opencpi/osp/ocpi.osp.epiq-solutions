.. z3u_i2c_bus0 HDL worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _z3u_i2c_bus0-HDL-worker:

``z3u_i2c_bus0`` HDL worker
===========================

Implementation of the z3u_i2c_bus0 HDL worker for the OpenCPI i2c state-machine.

Detail
------

The z3u_i2c_bus0 HDL subdevice worker is specific to the Matchstiq Z3U platform.
It instances the icm20602 component. This component utilizes raw properties to
access the bare metal device and is indexed according to the SLAVE_ADDRESS_p indices
located in the z3u_i2c_bus2-build.xml. The CLK_FREQ_p parameter declares the expected
frequency of the control plane clock.

Review of the Epiq Solutions Matchstiq-Z3u Hardware Users Manual determined that the
I2C SCL should be set to 100kHz.

The eeprom_24AA128 component is NOT included on the z3u_i2c_bus0. Work was initially done
to include the eeprom 24AA128 but an I2C addressing issue was encountered that hindered
the ability to implement this component. OpenCPI Ticket: #2662.


I2C Bus0 Addressing
-------------------

+------------------------+-----------+-----------+
| Worker                 | Index     | Address   |
|                        |           |           |
+========================+===========+===========+
| icm20602               | 0         | 0x62      |
+------------------------+-----------+-----------+


Control Timing and Signals
--------------------------

The z3u_i2c_bus0 device worker follows the same implementation scheme as other examples within FOSS. Which is to say that the expected control plane clock rate is 100MHz and the desired I2C SCL is determine by setting the CLK_CNT generic of the i2c_opencore_ctrl instance. For example, if the desired I2C SCL frequency is 100kHz, then the CLK_CNT is set per the following.

      ``CLK_CNT => to_unsigned(from_float(CLK_FREQ_p)/100000,16) ) -- I2C clk = ~ 100kHz``

.. image:: ./images/z3u_i2c_bus0.png
      :width: 1600

Issues
------

* FRAMEWORK BUG: Issue #2567: NUSERS_p value is ignored due to a bug described in the z3u_i2c-spec.xml and maintains the NUSERS_p default value of 4.
  ``<parameter name='NUSERS_p' value='4'/>``

* FRAMEWORK LIMITATION: Issue #2662: OCPI wrapper of I2C OpenCores does not support addresses greater than 8 bits.
