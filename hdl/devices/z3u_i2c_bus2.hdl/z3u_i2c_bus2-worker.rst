.. z3u_i2c_bus2 HDL worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _z3u_i2c_bus2-HDL-worker:

``z3u_i2c_bus2`` HDL worker
===========================

Implementation of the z3u_i2c_bus2 HDL worker for the OpenCPI i2c state-machine.

Detail
------

The z3u_i2c_bus2 HDL subdevice worker is specific to the Matchstiq Z3U platform.
It instances four workers on the bus, one sit5356 and three tmp103's.
Each of these workers utilize raw properties to access the bare metal device and
are indexed according to the SLAVE_ADDRESS_p indices located in the
z3u_i2c_bus2-build.xml. The CLK_FREQ_p parameter declares the expected frequency of the control plane clock.

Review of the Epiq Solutions Matchstiq-Z3u Hardware Users Manual determined that the I2C SCL rate should be set to 100kHz.

I2C Bus2 Addressing
-------------------

+------------------------+-----------+-----------+
| Worker                 | Index     | Address   |
|                        |           |           |
+========================+===========+===========+
| sit5356                | 0         | 0x62      |
+------------------------+-----------+-----------+
| tmp103                 | 1         | 0x70      |
+------------------------+-----------+-----------+
| tmp103                 | 2         | 0x71      |
+------------------------+-----------+-----------+
| tmp103                 | 3         | 0x72      |
+------------------------+-----------+-----------+

Control Timing and Signals
--------------------------

The z3u_i2c_bus2 device worker follows the same implementation scheme as other examples within FOSS. Which is to say that the expected control plane clock rate is 100MHz and the desired I2C SCL is determine by setting the CLK_CNT generic of the i2c_opencore_ctrl instance. For example, if the desired I2C SCL frequency is 100kHz, then the CLK_CNT is set per the following.

      ``CLK_CNT => to_unsigned(from_float(CLK_FREQ_p)/100000,16) ) -- I2C clk = ~ 100kHz``

.. image:: ./images/z3u_i2c_bus2.png
   :width: 1600
