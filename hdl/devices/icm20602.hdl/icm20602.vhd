
architecture rtl of icm20602_worker is
begin
  -- Control plane outputs.  Raw props routed to underlying I2C
  rawprops_out.present         <= '1';
  rawprops_out.reset           <= ctl_in.reset;
  rawprops_out.raw             <= props_in.raw;
  props_out.raw                <= rawprops_in.raw;
end rtl;
