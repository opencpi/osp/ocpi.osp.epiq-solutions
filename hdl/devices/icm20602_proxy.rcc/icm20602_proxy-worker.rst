.. icm20602 RCC worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _icm20602-RCC-worker:


``icm20602`` RCC worker
=======================

Implementation of the icm20602 RCC worker for the TDK InvenSense icm-20602 accelerometer.

Detail
------

The icm20602_proxy worker allows the user to gather accelerometer and gyroscopic data. The user has
the ability to set a Gyroscopic Sensitivity (gyro_sensitivity) and an Accelerometer Sensitivity
(accel_sensitivity) in order to fine-tune the output data. The user can read single data reads of
the Gyroscopic Data (gyro_data), Accelerometer Data (accel_data), and the Temperature Data
(temp_data). The user can also utilize the Continuous Data (continuous_data) mode by setting the
continuous_data property true in the application, this mode will read a continuous stream of
gyroscopic and accelerometer data.

References:

 * http://3cfeqx1hf82y3xcoull08ihx-wpengine.netdna-ssl.com/wp-content/uploads/2020/11/DS-000176-ICM-20602-v1.1.pdf
