
#include "icm20602_proxy-worker.hh"
#include <fstream>
#include <unistd.h>
#include "OcpiDebugApi.hh" // OCPI_LOG_DEBUG

using namespace OCPI::RCC; // for easy access to RCC data types and constants
using namespace Icm20602_proxyWorkerTypes;

class Icm20602_proxyWorker : public Icm20602_proxyWorkerBase {
  RunCondition m_aRunCondition;
public:
  Icm20602_proxyWorker() : m_aRunCondition(RCC_NO_PORTS) {
    //Run function should never be called
    setRunCondition(&m_aRunCondition);
  }
private:
  RCCResult run(bool /*timedout*/) {
    return RCC_DONE;
  }

  // Member Variables
  float m_gyro_sensitivity_factor;
  short m_accel_sensitivity_factor;
  short m_accel_x_data, m_accel_y_data, m_accel_z_data;
  short m_gyro_x_data, m_gyro_y_data, m_gyro_z_data;

  // Initialize the icm20602 accelerometer
  RCCResult init_config_read()
  {
    if (m_properties.init_config)
    {
      slave.set_pwr_mgmt_1(0x01);
      slave.set_pwr_mgmt_2(0x3F);
      slave.set_user_ctrl(0x00);
      slave.set_pwr_mgmt_2(0x00);
    }
    return RCC_OK;
  }

  // Sets the gyroscope sensitivity and associated scale factor
  RCCResult gyro_sensitivity_read()
  {
    unsigned int g_sensitivity = m_properties.gyro_sensitivity;

    switch (g_sensitivity)
    {
      case 250:
        slave.set_gyro_config(0x00);
        m_gyro_sensitivity_factor = 131;
        break;
      case 500:
        slave.set_gyro_config(0x08);
        m_gyro_sensitivity_factor = 65.5;
        break;
      case 1000:
        slave.set_gyro_config(0x10);
        m_gyro_sensitivity_factor = 32.8;
        break;
      case 2000:
        slave.set_gyro_config(0x18);
        m_gyro_sensitivity_factor = 16.4;
        break;
      default:
        return setError("\nError: Invalid gyroscope scale range!\n"
                        "Possible gyroscope scale range values:"
                         " 250, 500, 1000, 2000");
    }
    return RCC_OK;
  }

  // Sets the accelerometer sensitivity and associated scale factor
  RCCResult accel_sensitivity_read()
  {
    unsigned int a_sensitivity = m_properties.accel_sensitivity;

    switch (a_sensitivity)
    {
      case 2:
        slave.set_accel_config_1(0x00);
        m_accel_sensitivity_factor = 16384;
        break;
      case 4:
        slave.set_accel_config_1(0x08);
        m_accel_sensitivity_factor = 8192;
        break;
      case 8:
        slave.set_accel_config_1(0x10);
        m_accel_sensitivity_factor = 4096;
        break;
      case 16:
        slave.set_accel_config_1(0x18);
        m_accel_sensitivity_factor = 2048;
        break;
      default:
        return setError("\nError: Invalid accelerometer scale range!\n"
                        "Possible accelerometer scale range values:"
                         " 2, 4, 8, 16");
    }
    return RCC_OK;
  }

  // Gets the Gyroscopic data
  RCCResult gyro_data_read()
  {
    slave.set_pwr_mgmt_2(0x00);

    signed short gx, gy, gz;

    // Get Gyro data (x)
    signed short char_gyro_xout_h = slave.get_gyro_xout_h() << 8;
    signed short char_gyro_xout_l = slave.get_gyro_xout_l();
    signed short gyro_xout = char_gyro_xout_h | char_gyro_xout_l;

    gx = gyro_xout / m_gyro_sensitivity_factor; // raw data divided by scalor
    m_properties.gyro_data.x = gx;
    m_gyro_x_data = gx;

    // Get Gyro data (y)
    signed short char_gyro_yout_h = slave.get_gyro_yout_h() << 8;
    signed short char_gyro_yout_l = slave.get_gyro_yout_l();
    signed short gyro_yout = char_gyro_yout_h | char_gyro_yout_l;

    gy = gyro_yout / m_gyro_sensitivity_factor;
    m_properties.gyro_data.y = gy;
    m_gyro_y_data = gy;

    // Get Gyro data (z)
    signed short char_gyro_zout_h = slave.get_gyro_zout_h() << 8; signed short char_gyro_zout_l = slave.get_gyro_zout_l(); signed short gyro_zout = char_gyro_zout_h | char_gyro_zout_l;

    gz = gyro_zout / m_gyro_sensitivity_factor;
    m_properties.gyro_data.z = gz;
    m_gyro_z_data = gz;

    // Debugging print statement
    log(OCPI_LOG_DEBUG, "Info: Gyroscope: x=%d y=%d z=%d\n", m_properties.gyro_data.x, m_properties.gyro_data.y, m_properties.gyro_data.z);

    return RCC_OK;
  }

  // Gets the Accelerometer data
  RCCResult accel_data_read()
  {
    slave.set_pwr_mgmt_2(0x00);

    signed short ax, ay, az;

    // Get Accel data (x)
    signed short char_accel_xout_h = slave.get_accel_xout_h() << 8;
    signed short char_accel_xout_l = slave.get_accel_xout_l();
    signed short accel_xout = char_accel_xout_h | char_accel_xout_l;

    ax = accel_xout / m_accel_sensitivity_factor; // raw data divided by scalor
    m_properties.accel_data.x = ax;
    m_accel_x_data = ax;

    // Get Accel data (y)
    signed short char_accel_yout_h = slave.get_accel_yout_h() << 8;
    signed short char_accel_yout_l = slave.get_accel_yout_l();
    signed short accel_yout = char_accel_yout_h | char_accel_yout_l;

    ay = accel_yout / m_accel_sensitivity_factor;
    m_properties.accel_data.y = ay;
    m_accel_y_data = ay;

    // Get Accel data (z)
    signed short char_accel_zout_h = slave.get_accel_zout_h() << 8;
    signed short char_accel_zout_l = slave.get_accel_zout_l();
    signed short accel_zout = char_accel_zout_h | char_accel_zout_l;

    az = accel_zout / m_accel_sensitivity_factor;
    m_properties.accel_data.z = az;
    m_accel_z_data = az;

    // Debugging print statement
    log(OCPI_LOG_DEBUG, "Info: Accelerometer: x=%d y=%d z=%d\n", m_properties.accel_data.x, m_properties.accel_data.y, m_properties.accel_data.z);

    return RCC_OK;
  }

  // Gets the Temperature data
  RCCResult temp_data_read()
  {
    slave.set_pwr_mgmt_2(0x00);

    signed short temp_sensitivity = 326.8;
    signed short room_temp_offset = 25;

    // Get Temp data (x)
    signed short char_temp_h = slave.get_temp_out_h() << 8;
    signed short char_temp_l = slave.get_temp_out_l();
    signed short temp = char_temp_h | char_temp_l;

    m_properties.temp_data = (temp / temp_sensitivity) + room_temp_offset;

    // Debugging print statement
    log(OCPI_LOG_DEBUG, "Info: Temperature: %d\n", m_properties.temp_data);

    return RCC_OK;
  }

  // Displays a continuous read of the gyro/accel data.
  RCCResult continuous_data_read()
  {

    // Coninuous Mode Warning
    printf("WARNING: Continuous Mode will not work properly if the device is set up for server mode!");

    // stdout print refresh rate
    unsigned int microseconds = 100000;

    // Collect continuous_test bool from z3u_i2c_bus0_test.xml property value
    bool test = m_properties.continuous_data;

    // If continous_test_mode true
    if(test)
    {
      printf("Press ctrl + c to cancel\n");
      gyro_sensitivity_read();
      accel_sensitivity_read();
    }
    while(test)
    {
      gyro_data_read(); 
      accel_data_read(); 
      printf("\rInfo: Gyroscope: x=%-4d y=%-4d z=%-4d Info: Accelerometer: x=%-2d y=%-2d z=%-2d", m_gyro_x_data, m_gyro_y_data, m_gyro_z_data,
                                                                                                  m_accel_x_data, m_accel_y_data, m_accel_z_data);
      usleep(microseconds);
      fflush(stdout);
    }
    return RCC_OK;
  }
};

ICM20602_PROXY_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
ICM20602_PROXY_END_INFO
