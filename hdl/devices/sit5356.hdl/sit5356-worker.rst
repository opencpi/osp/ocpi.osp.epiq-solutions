.. sit5356 HDL worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _sit5356-HDL-worker:

``sit5356`` HDL worker
======================

Implementation of the sit5356 HDL worker for the SiTime sit5356 oscillator.

Detail
------

The sit5356 HDL worker uses the Raw Properties rawprops to interface with the
register map of the SiTime sit5356 Super-TCXO.

The SiT5356 is purchased with a fixed frequency value. This frequency can vary
from 1.0 MHz - 60.0 MHz and is determined by identifying the exact part number that
is used for the sit5356. The Ordering Information is located in the sit5356-datasheet.
As the Matchstiq Z3U was the first platform that required this device, the default
frequency is set to its requirements, i.e. 40.0 MHz.

References:
 * https://www.sitime.com/support/resource-library/datasheets/sit5356-datasheet

Control Timing and Signals
--------------------------

The si5356 device worker operates entirely in the control plane clock domain. All I2C SCL and SDA
signals are generated in the control plane domain. Note that the I2C SCL can only be a divided
version of the control plane clock.

Worker ports
------------

Inputs: ``n/a``

Outputs: ``n/a``
