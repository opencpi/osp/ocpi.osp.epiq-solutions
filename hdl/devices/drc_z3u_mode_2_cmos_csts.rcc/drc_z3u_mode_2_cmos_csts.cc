
#include "drc_z3u_mode_2_cmos_csts-worker.hh"

// These are the helper classes for the ad9361 helpers
#include "RadioCtrlrConfiguratorTuneResamp.hh"
#include "RadioCtrlrConfiguratorAD9361.hh"
#include "RadioCtrlrNoOSTuneResamp.hh"

using namespace OCPI::RCC; // for easy access to RCC data types and constants
using namespace Drc_z3u_mode_2_cmos_cstsWorkerTypes;

//This is for the generic drc helper class
#include "OcpiDrcProxyApi.hh" // this must be after the "using namespace" of the types namespace

namespace OD = OCPI::DRC;

enum class z3u_rx_band_t {b1, b2, b3, b4, b5, b6, b7, b8, b9, b10,
    b11, b12, b13, b14, b15, b16, b17, b18, b19, b20};

class Drc_z3u_mode_2_cmos_cstsWorker : public OD::DrcProxyBase {

  // ========================================================================
  // To use the ad9361 DRC helper classes, we need a configurator class that combines the bas ad9361
  // one with the tuneresamp soft tuning
  class z3u_mode_2_cmos_csts_Configurator: public OD::ConfiguratorAD9361, public OD::ConfiguratorTuneResamp {
  public:

    z3u_mode_2_cmos_csts_Configurator()
      : OD::ConfiguratorAD9361(DRC_Z3U_MODE_2_CMOS_CSTS_RF_PORTS_RX.data[0], NULL,
		               DRC_Z3U_MODE_2_CMOS_CSTS_RF_PORTS_TX.data[0], NULL),
	OD::ConfiguratorTuneResamp(ad9361MaxRxSampleMhz(), ad9361MaxTxSampleMhz()) {
    }
    // All concrete Configurator classes must have this clone method for virtual copying.
    OD::Configurator *clone() const { return new z3u_mode_2_cmos_csts_Configurator(*this); }
  protected:
    // The virtual callback to impose all constraints.
    void impose_constraints_single_pass() {
      ConfiguratorAD9361::impose_constraints_single_pass();
      ConfiguratorTuneResamp::impose_constraints_single_pass();
      Configurator::impose_constraints_single_pass();
    }
  } m_configurator;

  // ================================================================================================
  // trampoline between the DRC ad9361 helper classes in the framework library and the slave workers
  // accessible from this worker.  It lets the former call the latter
  struct DoSlave : OD::DeviceCallBack {
    Slaves &m_slaves;
    DoSlave(Slaves &slaves) : m_slaves(slaves) {}
    void get_byte(uint8_t /*id_no*/, uint16_t addr, uint8_t *buf) {
      m_slaves.config.getRawPropertyBytes(addr, buf, 1);
    }
    void set_byte(uint8_t /*id_no*/, uint16_t addr, const uint8_t *buf) {
      m_slaves.config.setRawPropertyBytes(addr, buf, 1);
    }
    void set_reset(uint8_t /*id_no*/, bool on) {
      m_slaves.config.set_force_reset(on ? 1 : 0);
    }
    bool isMixerPresent(bool rx, unsigned stream) {
      return stream == 0 && rx ? m_slaves.rx_complex_mixer0.isPresent() : false;
    }
    OD::config_value_t getDecimation(unsigned /*stream*/) {
      return m_slaves.rx_cic_dec0.isPresent() ? m_slaves.rx_cic_dec0.get_R() : 1;
    }
    OD::config_value_t getInterpolation(unsigned /*stream*/) {
      return m_slaves.tx_cic_int0.isPresent() ? m_slaves.tx_cic_int0.get_R() : 1;
    }
    OD::config_value_t getPhaseIncrement(bool rx, unsigned /*stream*/) {
      return rx ? m_slaves.rx_complex_mixer0.get_phs_inc() : 0;
    }
    void setPhaseIncrement(bool rx, unsigned /*stream*/, int16_t inc) {
      if (rx)
        m_slaves.rx_complex_mixer0.set_phs_inc(inc);
    }
    void initialConfig(uint8_t /*id_no*/, OD::Ad9361InitConfig &config) {
      OD::ad9361InitialConfig(m_slaves.config, m_slaves.data_sub, config);
    }
    void postConfig(uint8_t /*id_no*/) {
      OD::ad9361PostConfig(m_slaves.config);
    }
    void finalConfig(uint8_t /*id_no*/, OD::Ad9361InitConfig &config) {
      OD::ad9361FinalConfig(m_slaves.config, config);
    }

    // both of these apply to both channels on the ad9361
    // ---------------------------------------------------------------
    // COPIED FROM Z3UDRC.hh
    void assign_pcal_port_bit(size_t pcal, size_t port, size_t bit, bool set) {
      assert(port <= 2);
      assert(bit <= 7);
      assert((pcal == 2001) or (pcal == 2008) or (pcal == 2011));
      uint8_t val;
      if(port == 1) {
	if(pcal == 2001) {
	  val = m_slaves.pcal_2001.get_output_port_1();
	}
	else if(pcal == 2008) {
	  val = m_slaves.pcal_2008.get_output_port_1();
	}
	else { // 2011
	  val = m_slaves.pcal_2011.get_output_port_1();
	}
      }
      else {
	if(pcal == 2001) {
	  val = m_slaves.pcal_2001.get_output_port_2();
	}
	else if(pcal == 2008) {
	  val = m_slaves.pcal_2008.get_output_port_2();
	}
	else { // 2011
	  val = m_slaves.pcal_2011.get_output_port_2();
	}
      }
      uint8_t new_val = 0;
      if(set) {
	new_val = (bit == 0) ? (val | 0x01) : new_val;
	new_val = (bit == 1) ? (val | 0x02) : new_val;
	new_val = (bit == 2) ? (val | 0x04) : new_val;
	new_val = (bit == 3) ? (val | 0x08) : new_val;
	new_val = (bit == 4) ? (val | 0x10) : new_val;
	new_val = (bit == 5) ? (val | 0x20) : new_val;
	new_val = (bit == 6) ? (val | 0x40) : new_val;
	new_val = (bit == 7) ? (val | 0x80) : new_val;
      }
      else { // clear
	new_val = (bit == 0) ? (val & ~0x01) : new_val;
	new_val = (bit == 1) ? (val & ~0x02) : new_val;
	new_val = (bit == 2) ? (val & ~0x04) : new_val;
	new_val = (bit == 3) ? (val & ~0x08) : new_val;
	new_val = (bit == 4) ? (val & ~0x10) : new_val;
	new_val = (bit == 5) ? (val & ~0x20) : new_val;
	new_val = (bit == 6) ? (val & ~0x40) : new_val;
	new_val = (bit == 7) ? (val & ~0x80) : new_val;
      }
      if(port == 1) {
	printf("pcal:%ld port 1:0x%X\n", pcal,new_val);
	if(pcal == 2001) {
	  m_slaves.pcal_2001.set_output_port_1(new_val);
	}
	else if(pcal == 2008) {
	  m_slaves.pcal_2008.set_output_port_1(new_val);
	}
	else { // 2011
	  m_slaves.pcal_2011.set_output_port_1(new_val);
	}
      }
      else {
	printf("pcal:%ld port 2:0x%X\n", pcal,new_val);
	if(pcal == 2001) {
	  m_slaves.pcal_2001.set_output_port_2(new_val);
	}
	else if(pcal == 2008) {
	  m_slaves.pcal_2008.set_output_port_2(new_val);
	}
	else { // 2011
	  m_slaves.pcal_2011.set_output_port_2(new_val);
	}
      }
    }
    void set_bypass_z3u_rx_hpf(bool val) {
      val ?
	m_slaves.pcal_2008.set_output_port_2(0x40) :
	m_slaves.pcal_2008.set_output_port_2(0x80);
    }
    void set_z3u_rx_band_filters(z3u_rx_band_t band) {
      printf("Configure filter path\n");
      if(band == z3u_rx_band_t::b1) {
	m_slaves.pcal_2011.set_output_port_0(0x03);
	set_bypass_z3u_rx_hpf(true);
      }
      else if(band == z3u_rx_band_t::b2) {
	m_slaves.pcal_2011.set_output_port_0(0x35);
	set_bypass_z3u_rx_hpf(false);
      }
      else if(band == z3u_rx_band_t::b3) {
	m_slaves.pcal_2011.set_output_port_0(0x66);
	set_bypass_z3u_rx_hpf(false);
      }
      else if(band == z3u_rx_band_t::b4) {
	m_slaves.pcal_2011.set_output_port_0(0x77);
	set_bypass_z3u_rx_hpf(false);
      }
      else if(band == z3u_rx_band_t::b5) {
	m_slaves.pcal_2011.set_output_port_0(0xa8);
	set_bypass_z3u_rx_hpf(false);
      }
      else if(band == z3u_rx_band_t::b6) {
	m_slaves.pcal_2011.set_output_port_0(0xb9);
	set_bypass_z3u_rx_hpf(false);
      }
      else if(band == z3u_rx_band_t::b7) {
	m_slaves.pcal_2011.set_output_port_0(0xda);
	set_bypass_z3u_rx_hpf(false);
      }
      else if(band == z3u_rx_band_t::b8) {
	m_slaves.pcal_2011.set_output_port_0(0xeb);
	set_bypass_z3u_rx_hpf(false);
      }
      else if(band == z3u_rx_band_t::b9) {
	m_slaves.pcal_2011.set_output_port_0(0xec);
	set_bypass_z3u_rx_hpf(false);
      }
      else if(band == z3u_rx_band_t::b10) {
	m_slaves.pcal_2011.set_output_port_0(0xff);
	set_bypass_z3u_rx_hpf(false);
      }
      else if(band == z3u_rx_band_t::b11) {
	m_slaves.pcal_2011.set_output_port_1(0x21);
      }
      else if(band == z3u_rx_band_t::b12) {
	m_slaves.pcal_2011.set_output_port_1(0x88);
      }
      else if(band == z3u_rx_band_t::b13) {
	m_slaves.pcal_2011.set_output_port_1(0xdc);
      }
      else if(band == z3u_rx_band_t::b14) {
	m_slaves.pcal_2011.set_output_port_1(0xff);
      }
      else if(band == z3u_rx_band_t::b15) {
	m_slaves.pcal_2001.set_output_port_0(0x81);
      }
      else if(band == z3u_rx_band_t::b16) {
	m_slaves.pcal_2001.set_output_port_0(0xda);
      }
      else if(band == z3u_rx_band_t::b17) {
	m_slaves.pcal_2001.set_output_port_0(0xfe);
	printf("DRC: PCAL_2001 configured!\n");
      }
      else if(band == z3u_rx_band_t::b18) {
	m_slaves.pcal_2001.set_output_port_1(0x70);
      }
      else if(band == z3u_rx_band_t::b19) {
	m_slaves.pcal_2001.set_output_port_1(0xc9);
      }
      else if(band == z3u_rx_band_t::b20) {
	m_slaves.pcal_2001.set_output_port_1(0xfe);
      }
    }
    void set_z3u_rx_sma_rx_band(z3u_rx_band_t band) {
      printf("Select RX2 port of AD9361 and RF path\n");
      if((size_t)band <= (size_t)z3u_rx_band_t::b10) {
	assign_pcal_port_bit(2001, 2, 1, 1); // Rx U1302/U1304
	assign_pcal_port_bit(2008, 2, 1, 1); // RX U1402/U1401
	assign_pcal_port_bit(2008, 2, 4, 0);
	assign_pcal_port_bit(2008, 2, 0, 0);
	assign_pcal_port_bit(2001, 2, 2, 0); // Rx U1503/U1505
	assign_pcal_port_bit(2001, 2, 6, 1); // Rx U1502
      }
      else if(((size_t)band >= (size_t)z3u_rx_band_t::b11) and
	      ((size_t)band <= (size_t)z3u_rx_band_t::b14)) {
	assign_pcal_port_bit(2001, 2, 1, 1); // Rx U1302/U1304
	assign_pcal_port_bit(2008, 2, 1, 0); // RX U1402/U1401
	assign_pcal_port_bit(2008, 2, 4, 0);
	assign_pcal_port_bit(2008, 2, 0, 1);
	assign_pcal_port_bit(2008, 2, 2, 1); // Rx U1404/U1403
	assign_pcal_port_bit(2008, 2, 3, 0);
	assign_pcal_port_bit(2001, 2, 2, 0); // Rx U1503/U1505
	if((size_t)band <= (size_t)z3u_rx_band_t::b12) {
	  assign_pcal_port_bit(2001, 2, 6, 1); // Rx U1502
	}
	else {
	  assign_pcal_port_bit(2001, 2, 7, 1); // Rx U1502
	}
      }
      else if(((size_t)band >= (size_t)z3u_rx_band_t::b15) and
	      ((size_t)band <= (size_t)z3u_rx_band_t::b17)) {
	assign_pcal_port_bit(2001, 2, 1, 1); // Rx U1302/U1304
	assign_pcal_port_bit(2008, 2, 1, 0); // RX U1402/U1401
	assign_pcal_port_bit(2008, 2, 4, 0);
	assign_pcal_port_bit(2008, 2, 0, 1);
	assign_pcal_port_bit(2001, 2, 2, 0); // Rx U1404/U1403
	assign_pcal_port_bit(2001, 2, 3, 1);
	assign_pcal_port_bit(2001, 2, 7, 1); // Rx U1502
      }
      else if((size_t)band >= (size_t)z3u_rx_band_t::b18) {
	assign_pcal_port_bit(2001, 2, 1, 1); // Rx U1302/U1304
	assign_pcal_port_bit(2008, 2, 1, 0); // RX U1402/U1401
	assign_pcal_port_bit(2008, 2, 4, 0);
	assign_pcal_port_bit(2008, 2, 0, 1);
	assign_pcal_port_bit(2001, 2, 2, 0); // Rx U1404/U1403
	assign_pcal_port_bit(2001, 2, 3, 1);
	assign_pcal_port_bit(2001, 2, 7, 1); // Rx U1502
      }
    }
    void set_z3u_trx_sma_rx_band(z3u_rx_band_t band) {
      printf("Select TRX1 port of AD9361 and RF path\n");
      if((size_t)band <= (size_t)z3u_rx_band_t::b10) {
	assign_pcal_port_bit(2008, 1, 1, 1); // TRx U1603/U1605
	assign_pcal_port_bit(2008, 2, 1, 1); // TRX U1702/U1701
	assign_pcal_port_bit(2008, 2, 4, 0);
	assign_pcal_port_bit(2008, 2, 0, 0);
	assign_pcal_port_bit(2008, 1, 2, 0); // TRx U1803/U1805
	assign_pcal_port_bit(2008, 1, 6, 1); // TRx U1802
      }
      else if(((size_t)band >= (size_t)z3u_rx_band_t::b11) and
	      ((size_t)band <= (size_t)z3u_rx_band_t::b14)) {
	assign_pcal_port_bit(2008, 1, 1, 1); // TRx U1603/U1605
	assign_pcal_port_bit(2008, 2, 1, 0); // TRX U1702/U1701
	assign_pcal_port_bit(2008, 2, 4, 0);
	assign_pcal_port_bit(2008, 2, 0, 1);
	assign_pcal_port_bit(2008, 2, 2, 1); // TRx U1704/U1703
	assign_pcal_port_bit(2008, 2, 3, 0);
	assign_pcal_port_bit(2008, 1, 2, 0); // TRx U1803/U1805
	if((size_t)band <= (size_t)z3u_rx_band_t::b12) {
	  assign_pcal_port_bit(2008, 1, 6, 1); // TRx U1802
	}
	else {
	  assign_pcal_port_bit(2008, 1, 7, 1); // TRx U1802
	}
      }
      else if(((size_t)band >= (size_t)z3u_rx_band_t::b15) and
	      ((size_t)band <= (size_t)z3u_rx_band_t::b17)) {
	assign_pcal_port_bit(2008, 1, 1, 1); // TRx U1603/U1605
	assign_pcal_port_bit(2008, 2, 1, 0); // TRX U1702/U1701
	assign_pcal_port_bit(2008, 2, 4, 0);
	assign_pcal_port_bit(2008, 2, 0, 1);
	assign_pcal_port_bit(2008, 2, 2, 0); // TRx U1704/U1703
	assign_pcal_port_bit(2008, 2, 3, 1);
	assign_pcal_port_bit(2008, 1, 7, 1); // TRx U1802
      }
      else if((size_t)band >= (size_t)z3u_rx_band_t::b18) {
	assign_pcal_port_bit(2008, 1, 1, 1); // TRx U1603/U1605
	assign_pcal_port_bit(2008, 2, 1, 0); // TRX U1702/U1701
	assign_pcal_port_bit(2008, 2, 4, 1);
	assign_pcal_port_bit(2008, 2, 0, 0);
	assign_pcal_port_bit(2008, 1, 2, 0); // TRx U1803/U1805
	assign_pcal_port_bit(2008, 1, 5, 1); // TRx U1802
      }
    }
    void set_z3u_trx_sma_tx_band(bool freq_is_greater_than_3000) {
      if(freq_is_greater_than_3000) {
	printf("DRC: TX is greater than 3000\n");
      }
      else {
	printf("DRC: TX is less than 3000\n");
	}
    }
    void set_z3u_rx_band(z3u_rx_band_t band) {
      set_z3u_rx_band_filters(band);
      set_z3u_rx_sma_rx_band(band);
      set_z3u_trx_sma_rx_band(band);
    }
    void set_z3u_tx_band(bool freq_is_greater_than_3000) {
      set_z3u_trx_sma_tx_band(freq_is_greater_than_3000);
      return;
    }

    // both of these apply to both channels on the ad9361
    unsigned getRfInput(unsigned /*device*/, OD::config_value_t tuning_freq_MHz) {

      OD::config_value_t val = tuning_freq_MHz;

      printf("DRC: RX tuning_freq_MHz: %lf\n", val);

      bool do_rx = true;

      if(do_rx) {
	if((val >= 47.) && (val < 135.)) {
	  set_z3u_rx_band(z3u_rx_band_t::b1);
	}
	else if((val >= 135.) && (val < 145.)) {
	  set_z3u_rx_band(z3u_rx_band_t::b2);
	}
	else if((val >= 145.) && (val < 150.)) {
	  set_z3u_rx_band(z3u_rx_band_t::b3);
	}
	else if((val >= 150.) && (val < 162.)) {
	  set_z3u_rx_band(z3u_rx_band_t::b4);
	}
	else if((val >= 162.) && (val < 175.)) {
	  set_z3u_rx_band(z3u_rx_band_t::b5);
	}
	else if((val >= 175.) && (val < 190.)) {
	  set_z3u_rx_band(z3u_rx_band_t::b6);
	}
	else if((val >= 190.) && (val < 212.)) {
	  set_z3u_rx_band(z3u_rx_band_t::b7);
	}
	else if((val >= 212.) && (val < 230.)) {
	  set_z3u_rx_band(z3u_rx_band_t::b8);
	}
	else if((val >= 230.) && (val < 280.)) {
	  set_z3u_rx_band(z3u_rx_band_t::b9);
	}
	else if((val >= 280.) && (val < 366.)) {
	  set_z3u_rx_band(z3u_rx_band_t::b10);
	}
	else if((val >= 366.) && (val < 475.)) {
	  set_z3u_rx_band(z3u_rx_band_t::b11);
	}
	else if((val >= 475.) && (val < 625.)) {
	  set_z3u_rx_band(z3u_rx_band_t::b12);
	}
	else if((val >= 625.) && (val < 800.)) {
	  set_z3u_rx_band(z3u_rx_band_t::b13);
	}
	else if((val >= 800.) && (val < 1175.)) {
	  set_z3u_rx_band(z3u_rx_band_t::b14);
	}
	else if((val >= 1175.) && (val < 1500.)) {
	  set_z3u_rx_band(z3u_rx_band_t::b15);
	}
	else if((val >= 1500.) && (val < 2100.)) {
	  set_z3u_rx_band(z3u_rx_band_t::b16);
	}
	else if((val >= 2100.) && (val < 2775.)) {
	  printf("DRC: band winner!\n");
	  set_z3u_rx_band(z3u_rx_band_t::b17);
	}
	else if((val >= 2775.) && (val < 3360.)) {
	  set_z3u_rx_band(z3u_rx_band_t::b18);
	}
	else if((val >= 3360.) && (val < 4600.)) {
	  set_z3u_rx_band(z3u_rx_band_t::b19);
	}
	else if((val >= 4600.) && (val < 6000.)) {
	  set_z3u_rx_band(z3u_rx_band_t::b20);
	}
      }
      //else { // tx
      set_z3u_tx_band(val > 3000.);
      //}

      //return 0;

      if((val >= 2775.) && (val <= 6000.)) {
	return A_BALANCED;
      }
      else if((val >= 75.) && (val <= 625.)) {
	return B_BALANCED;
      }
      else if((val > 625.) && (val < 2775.)) {
	return C_BALANCED;
      }
      else { //invalid frequency
	return 1;
      }

    }
    unsigned getRfOutput(unsigned /*device*/, OD::config_value_t tuning_freq_MHz) {

      OD::config_value_t val = tuning_freq_MHz;

      printf("DRC: TX tuning_freq_MHz: %lf\n", val);

      if((val > 3000)) {
	return TXA;
      }
      else {
	return TXB;
      }
      //return 0;
    }

  } m_doSlave;

  OD::RadioCtrlrNoOSTuneResamp m_ctrlr;
  OD::ConfigLockRequest m_requests[DRC_Z3U_MODE_2_CMOS_CSTS_MAX_CONFIGURATIONS_P];

public:
  Drc_z3u_mode_2_cmos_cstsWorker()
    : m_doSlave(slaves),
      m_ctrlr(0, "drc_z3u_mode_2_cmos_csts", m_configurator, m_doSlave) {
  }
  // ================================================================================================
  // These methods interface with the helper 9361 classes etc.
  // ================================================================================================
  RCCResult prepare_config(unsigned config) {
    log(8, "DRC: prepare_config: %u", config);
    auto &conf = m_properties.configurations.data[config];
    auto &req = m_requests[config];
    // So here we basically convert the data structure dictated by the drc spec property to the one
    // defined by the older DRC/ad9361 helper classes
    auto nChannels = conf.channels.length;
    req.m_data_streams.resize(nChannels);
    unsigned nRx = 0, nTx = 0;

    for (unsigned n = 0; n < nChannels; ++n) {
      auto &channel = conf.channels.data[n];
      auto &stream = req.m_data_streams[n];
      stream.include_data_stream_type(channel.rx ?
                                      OD::data_stream_type_t::RX : OD::data_stream_type_t::TX);
      stream.include_data_stream_ID(channel.rx ?
                                    DRC_Z3U_MODE_2_CMOS_CSTS_RF_PORTS_RX.data[nRx] :
                                    DRC_Z3U_MODE_2_CMOS_CSTS_RF_PORTS_TX.data[nTx]);
      stream.include_routing_ID((channel.rx ? "RX" : "TX") +
                                std::to_string(channel.rx ? nRx : nTx));
      ++(channel.rx ? nRx : nTx);
      stream.include_tuning_freq_MHz(channel.tuning_freq_MHz, channel.tolerance_tuning_freq_MHz);
      stream.include_bandwidth_3dB_MHz(channel.bandwidth_3dB_MHz, channel.tolerance_bandwidth_3dB_MHz);
      stream.include_sampling_rate_Msps(channel.sampling_rate_Msps, channel.tolerance_sampling_rate_Msps);
      stream.include_samples_are_complex(channel.samples_are_complex);
      stream.include_gain_mode(channel.gain_mode);
      if (!stream.get_gain_mode().compare("manual"))
        stream.include_gain_dB(channel.gain_dB, channel.tolerance_gain_dB);
    }
    // Ideally we would validate them here, but not now.
    return RCC_OK;
  }
  RCCResult start_config(unsigned config) {
    log(8, "DRC: start_config: %u", config);

    try {
      return m_ctrlr.request_config_lock(std::to_string(config), m_requests[config]) ?
        RCC_OK :
        setError("config lock request was unsuccessful, set OCPI_LOG_LEVEL to 8 "
                 "(or higher) for more info");
    } catch(const char* err) {
      return setError(err);
    }
    return RCC_OK;
  }
  RCCResult stop_config(unsigned config) {
    log(8, "DRC: stop_config: %u", config);

    // Unlock the configurations when stopping the DRC.
    m_ctrlr.unlock_all();
    log(8, "DRC: stop_config -> Issued unlock for all configurations");

    return RCC_OK;
  }
  RCCResult release_config(unsigned /*config*/) {
    log(8, "DRC: release_config");
    return m_ctrlr.shutdown() ?
      setError("transceiver shutdown was unsuccessful, set OCPI_LOG_LEVEL to 8 "
               "(or higher) for more info") : RCC_OK;
  }
};

DRC_Z3U_MODE_2_CMOS_CSTS_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
DRC_Z3U_MODE_2_CMOS_CSTS_END_INFO
