.. tmp103 RCC worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _tmp103-RCC-worker:


``tmp103`` RCC worker
=====================

Implementation of the tmp103 RCC worker for the Texas Instruments tmp103
Digital Temperature Sensor.

Detail
------

The tmp103 RCC worker translates the tmp103 temperature register value into degrees Celsius.

The tmp103 RCC worker also defines temp_high_threshold, temp_low_threshold and
config properties that allows the user to define a maximum and minimum temperature
threshold and set the config_reg. The proxy then checks for appropriate values
and sets the thigh_reg and tlow_reg accordingly.

References:

 * https://www.ti.com/lit/ds/symlink/tmp103.pdf?ts=1631117992048&ref_url=https%253A%252F%252Fwww.google.com%252F
