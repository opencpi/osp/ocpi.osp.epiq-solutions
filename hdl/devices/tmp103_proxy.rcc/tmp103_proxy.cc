
#include "tmp103_proxy-worker.hh"
#include <sstream>
#include "OcpiDebugApi.hh" // OCPI_LOG_DEBUG

using namespace OCPI::RCC; // for easy access to RCC data types and constants
using namespace Tmp103_proxyWorkerTypes;

class Tmp103_proxyWorker : public Tmp103_proxyWorkerBase {
  RunCondition m_aRunCondition;
public:
  Tmp103_proxyWorker() : m_aRunCondition(RCC_NO_PORTS) {
    //Run function should never be called
    setRunCondition(&m_aRunCondition);
  }
private:
  RCCResult run(bool /*timedout*/) {
    return RCC_DONE;
  }

  // Set the temperature property using the tmp103 temp_reg register
  RCCResult temperature_read()
  {
    m_properties.temperature = slave.get_temp_reg();
    return RCC_OK;
  }

  // Checks the temp_low_threshold value and sets tlow_reg accordingly
  RCCResult temp_low_threshold_read()
  {
    if((m_properties.temp_low_threshold < -40) || (m_properties.temp_low_threshold > 125))
    {
      return setError("\nError: temp_low_threshold must be between -40 and 125 degrees C!");
    }
    else if(m_properties.temp_low_threshold > m_properties.temp_high_threshold)
    {
      return setError("\nError: temp_low_threshold must be less than temp_high_threshold!");
    }
    else
    {
      slave.set_tlow_reg(m_properties.temp_low_threshold);
    }
    return RCC_OK;
  }

  // Checks the temp_high_threshold value and sets thigh_reg accordingly
  RCCResult temp_high_threshold_read()
  {
    if((m_properties.temp_high_threshold < -40) || (m_properties.temp_high_threshold > 125))
    {
      return setError("\nError: temp_high_threshold must be between -40 and 125 degrees C!");
    }
    else if(m_properties.temp_high_threshold < m_properties.temp_low_threshold)
    {
      return setError("\nError: temp_high_threshold must be greater than temp_low_threshold!");
    }
    else
    {
      slave.set_thigh_reg(m_properties.temp_high_threshold);
    }
    return RCC_OK;
  }

  // Set the config_reg based on the config property
  RCCResult config_read()
  {
    slave.set_config_reg(m_properties.config);
    return RCC_OK;
  }
};

TMP103_PROXY_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
TMP103_PROXY_END_INFO
