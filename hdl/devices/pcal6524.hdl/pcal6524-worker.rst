.. pcal6524 HDL worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _pcal6524-HDL-worker:


``pcal6524`` HDL worker
=======================

Implementation of the pcal6524 HDL worker for the NXP pcal6524 24-bit general purpose I/O expander.

Detail
------

The pcal6524 HDL worker uses the Raw Properties rawprops to interface with the register map of the
pcal6524 NXP pcal6524 24-bit general purpose I/O expander.

References:

 * https://www.nxp.com/docs/en/data-sheet/PCAL6524.pdf

Control Timing and Signals
--------------------------

The pcal6524 device worker operates entirely in the control plane clock domain. All of the I2C SCL
and SDA signals are generated in the control plane domain. Note that the I2C SCL can only be a
divided version of the control plane clock.

Worker ports
------------

Inputs: ``n/a``

Outputs: ``n/a``
