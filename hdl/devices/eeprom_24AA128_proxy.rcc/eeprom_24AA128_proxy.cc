
// The eeprom_24AA128 Device Worker Proxy OCS has not been finished. Due to
// an I2C addressing issue that was encountered. OpenCPI Ticket: 2662. If the
// ticket is addressed proxy property elements should be added to the
// eeprom_24AA128_proxy-spec.xml that would represent the fragmented registers
// of the eeprom_24AA128 component spec, such as part_number, board_revision,
// variant, ref_clk_info, and ext_ref_clk_freq. Once those property elements have
// been implemented the eeprom_24AA128.cc should concatenate the data of the
// eeprom_24AA128 registers and display them to the user in a smart way.

#include "eeprom_24AA128_proxy-worker.hh"
#include <fstream>
#include <unistd.h>

using namespace OCPI::RCC; // for easy access to RCC data types and constants
using namespace Eeprom_24AA128_proxyWorkerTypes;

class Eeprom_24AA128_proxyWorker : public Eeprom_24AA128_proxyWorkerBase {
  RunCondition m_aRunCondition;
public:
  Eeprom_24AA128_proxyWorker() : m_aRunCondition(RCC_NO_PORTS) {
    //Run function should never be called
    setRunCondition(&m_aRunCondition);
  }
private:
  RCCResult run(bool /*timedout*/) {

    return RCC_DONE;
  }
};

EEPROM_24AA128_PROXY_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
EEPROM_24AA128_PROXY_END_INFO
