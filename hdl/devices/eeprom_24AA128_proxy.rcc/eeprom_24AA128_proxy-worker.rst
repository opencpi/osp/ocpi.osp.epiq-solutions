.. eeprom_24AA128 RCC worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _eeprom_24AA128-RCC-worker:


``eeprom_24AA128`` RCC worker
=============================

Implementation of the eeprom RCC worker for the MicroChip 24AA128

Detail
------

The eeprom_24AA128 Device Worker Proxy OCS has not been finished. Due to an I2C addressing issue
that was encountered. OpenCPI Ticket: 2662.

If the ticket is addressed proxy property elements should be added to the eeprom_24AA128_proxy-spec
that would represent the fragmented registers of the eeprom_24AA128 component spec, such as
part_number, board_revision, variant, ref_clk_info, and ext_ref_clk_freq. Once those property
elements are implemented, the eeprom_24AA128.cc can concatenate the data of the eeprom_24AA128
registers and display that information to the user is a smart way.

References:

 * http://ww1.microchip.com/downloads/en/devicedoc/21191s.pdf

Issue
-----

OpenCPI Ticket: #2662

OCPI wrapper of I2C OpenCores does not support addresses greater than 8 bits
