
#include "sit5356_proxy-worker.hh"
#include "stdlib.h"
#include "math.h"
#include "OcpiDebugApi.hh" // OCPI_LOG_DEBUG

using namespace OCPI::RCC; // for easy access to RCC data types and constants
using namespace Sit5356_proxyWorkerTypes;

class Sit5356_proxyWorker : public Sit5356_proxyWorkerBase {
  RunCondition m_aRunCondition;
public:
  Sit5356_proxyWorker() : m_aRunCondition(RCC_NO_PORTS) {
    //Run function should never be called
    setRunCondition(&m_aRunCondition);
  }
private:
  RCCResult run(bool /*timedout*/) {
    return RCC_DONE;
  }

  // Checks that a valid ppm_shift_from_nominal value has been set and set
  RCCResult ppm_shift_from_nominal_read()
  {
    if(abs(m_properties.ppm_shift_from_nominal) > m_properties.dig_pull_range_ctrl)
    {
      return setError("Nominal pull range must be between the digital pull range value.");
      return RCC_OK;
    }
    return RCC_OK;
  }

  // Checks that a valid pull range has been set and sets the
  // digital pull range control register accordingly
  RCCResult dig_pull_range_ctrl_read()
  {
    float array [] = {6.25,10,12.5,25,50,80,100,125,150,200,400,600,800,1200,1600,3200};

    for(int i = 0; i <= 16; i++)
    {
      if(array[i] == m_properties.dig_pull_range_ctrl)
      {
        slave.set_dig_pull_range_ctrl_reg(i);
        break;
      }
      if(i == 16)
      {
        return setError("\nError: Invalid pull range!\n"
                        "Possible range values:"
                        " 6.25,10,12.5,25,50,80,100,125,150,200,400,600,800,1200,1600,3200");
      }
    }

    // Byte Swapped OCPI_LOG_INFO Statements for dig_pull_range_ctrl_reg for the issue
    // seen here: OpenCPI Issue: #2663
    short dig_pull_range_ctrl_reg_upper = slave.get_dig_pull_range_ctrl_reg() << 8;
    short dig_pull_range_ctrl_reg_lower = slave.get_dig_pull_range_ctrl_reg() >> 8;
    short dig_pull_range_ctrl_reg_actual = dig_pull_range_ctrl_reg_upper | dig_pull_range_ctrl_reg_lower;
    printf("Byte Swapped dig_pull_range_ctrl_reg decimal: %d\n", dig_pull_range_ctrl_reg_actual);
    printf("Byte Swapped dig_pull_range_ctrl_reg hex: 0x%04x\n", dig_pull_range_ctrl_reg_actual);

    return RCC_OK;
  }

  // Calculates the Control Word Value and sets the Frequency Control
  // MSW and LSW registers
  RCCResult ctrl_word_value_read()
  {

    float f_digital_pull_range = m_properties.dig_pull_range_ctrl;
    float f_ppm_shift_from_nominal = m_properties.ppm_shift_from_nominal;
    bool is_positive;

    // Checks for positive Control Word Value
    if (f_ppm_shift_from_nominal >= 0)
    {
      is_positive = true;
    }
    else
    {
      is_positive = false;
    }

    // If Control Word Value is POSITIVE
    // (1) Control Word Value Calculation with the
    // pull_range = +/- 200 , and the  ppm_shift_from_nominal = 200
    // 33554431   = 0000_0001_1111_1111_|_1111_1111_1111_1111
    if(is_positive)
    {
      // (1) Control Word Value Calculation
      double d_pos_control_word_value;
      int pos_control_word_value;
      d_pos_control_word_value = ((pow(2, 25) -1) * (double(f_ppm_shift_from_nominal) / double(f_digital_pull_range)));
      pos_control_word_value = round(d_pos_control_word_value);
      m_properties.ctrl_word_value = pos_control_word_value;
      // (2) Set 16-bit Digital Frequency lsw Register
      // lsw         = 0000_0001_1111_1111_|[1111_1111_1111_1111] 0xFFFF
      slave.set_dig_freq_ctrl_lsw_reg(pos_control_word_value);
      // (3) Bit shift 22 bits and xor with 0xFFFFFC00 in order
      // to set 16-bit Digital Frequency msw Register
      // 33554431    = 0000_0001_1111_1111_|_1111_1111_1111_1111
      // shift 22    = 0000_0000_0000_0000_|_0000_0011_1111_1111
      // msw         = 0000_0000_0000_0000_|[0000_0011_1111_1111]  0x03FF
      slave.set_dig_freq_ctrl_msw_reg(pos_control_word_value >> 15);
    }
    // If Control Word Value is NEGATIVE
    // (1) Control Word Value Calculation with the
    // pull_range = +/- 200 , and the  ppm_shift_from_nominal = -200
    // -33554432   = 1111_1110_0000_0000_|_0000_0000_0000_0000
    else
    {
      // (1) Control Word Value Calculation
      double d_neg_control_word_value;
      int neg_control_word_value;
      d_neg_control_word_value = ((pow(2, 25)) * (double(f_ppm_shift_from_nominal) / double(f_digital_pull_range)));
      neg_control_word_value = round(d_neg_control_word_value);
      m_properties.ctrl_word_value = neg_control_word_value;
      // (2) Bit shift 2^6 to represent 2^26 two's compliment
      // -33554432 * 2^6 = -2147483648
      // -2147483648 = 1000_0000_0000_0000_|_0000_0000_0000_0000
      neg_control_word_value = ((pow(2, 6) * neg_control_word_value));
      // (3) Set 16-bit Digital Frequency lsw Register
      // lsw         = 1000_0000_0000_0000_|[0000_0000_0000_0000] 0x0000
      slave.set_dig_freq_ctrl_lsw_reg(neg_control_word_value);
      // (4) Bit shift 22 bits and xor with 0xFFFFFC00 in order
      // to set 16-bit Digital Frequency msw Register
      // -2147483648 = 1000_0000_0000_0000_|_0000_0000_0000_0000
      // shift 22    = 1111_1111_1111_1111_|_1111_1110_0000_0000
      // xor         = 1111_1111_1111_1111_|_1111_1100_0000_0000  0xFFFFFC00
      // msw         = 0000_0000_0000_0000_|[0000_0010_0000_0000]  0x0200
      slave.set_dig_freq_ctrl_msw_reg((neg_control_word_value >> 22) ^ 0xFFFFFC00);
    }

    // Byte Swapped OCPI_LOG_INFO Statements for dig_freq_ctrl_lsw_reg for the issue
    // seen here: OpenCPI Issue: #2663
    short dig_freq_ctrl_lsw_reg_upper = slave.get_dig_freq_ctrl_lsw_reg() << 8;
    short dig_freq_ctrl_lsw_reg_lower = slave.get_dig_freq_ctrl_lsw_reg() >> 8;
    short dig_freq_ctrl_lsw_reg_actual = dig_freq_ctrl_lsw_reg_upper | dig_freq_ctrl_lsw_reg_lower;
    printf("Byte Swapped dig_freq_ctrl_lsw_reg decimal: %d\n", dig_freq_ctrl_lsw_reg_actual);
    printf("Byte Swapped dig_freq_ctrl_lsw_reg hex: 0x%04X\n", dig_freq_ctrl_lsw_reg_actual & 0xffff);

    // Byte Swapped OCPI_LOG_INFO Statements for dig_freq_ctrl_msw_reg for the issue
    // seen here: OpenCPI Issue: #2663
    short dig_freq_ctrl_msw_reg_upper = slave.get_dig_freq_ctrl_msw_reg() << 8;
    short dig_freq_ctrl_msw_reg_lower = slave.get_dig_freq_ctrl_msw_reg() >> 8;
    short dig_freq_ctrl_msw_reg_actual = dig_freq_ctrl_msw_reg_upper | dig_freq_ctrl_msw_reg_lower;
    printf("Byte Swapped dig_freq_ctrl_msw_reg decimal: %d\n", dig_freq_ctrl_msw_reg_actual);
    printf("Byte Swapped dig_freq_ctrl_msw_reg hex: 0x%04X\n", dig_freq_ctrl_msw_reg_actual & 0xffff);

    return RCC_OK;
  }

  // Calculates the expected shifted frequency
  RCCResult shifted_frequency_read()
  {
    double ppm;
    // ppm = (frequency / 1,000,000) * frequency_shift
    ppm = (slave.get_frequency() / 1000000) * m_properties.ppm_shift_from_nominal;
    m_properties.shifted_frequency = slave.get_frequency() + ppm;

    return RCC_OK;
  }
};

SIT5356_PROXY_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
SIT5356_PROXY_END_INFO
